//
//  Move.swift
//  Chess Openings
//
//  Created by Casey Evanish on 8/27/17.
//  Copyright © 2017 Casey Evanish. All rights reserved.
//

import Foundation

public enum PieceType {
    case pawn, knight, bishop, rook, queen, king
}

public enum MoveType: Equatable {
    case normal
    case take(piece: PieceInfo, at: Coordinate)
    case promotion(selection: PromotionSelection, color: Color, takenPiece: PieceInfo?, takenCoord: Coordinate?)
    case castle(direction: CastleDirection)
    
    public static func == (lhs: MoveType, rhs: MoveType) -> Bool {
        switch (lhs, rhs) {
        case (.normal, .normal):
            return true
        case let (.take(piece1, coordinate1), .take(piece2, coordinate2)):
            return piece1.type == piece2.type && coordinate1 == coordinate2
            //return piece1.isEqual(to: piece2) && coordinate1 == coordinate2
        case let (.promotion(selection1, color1, takenPiece1, coordinate1),
                  .promotion(selection2, color2, takenPiece2, coordinate2)):
            var takenEqual = true
            if let takenPiece1 = takenPiece1, let takenPiece2 = takenPiece2 {
                takenEqual = takenPiece1.type == takenPiece2.type
                //takenEqual = takenPiece1.isEqual(to: takenPiece2)
            }
            return selection1 == selection2 && takenEqual && coordinate1 == coordinate2 && color1 == color2
        case let (.castle(direction1), .castle(direction2)):
            return direction1 == direction2
        default:
            return false
        }
    }
}

public struct SingleChessMove: Equatable {
    
    public var pieceInfo: PieceInfo
    public var from: Coordinate
    public var to: Coordinate
    public var type: MoveType
    //this is here for FEN purposes
    public var halfMoveNum: Int
    
    init(pieceInfo: PieceInfo, from: Coordinate, to: Coordinate, type: MoveType, halfMoveNum: Int) {
        self.from = from
        self.to = to
        self.type = type
        self.halfMoveNum = halfMoveNum
        self.pieceInfo = pieceInfo
    }
    
    public static func ==(lhs: SingleChessMove, rhs: SingleChessMove) -> Bool {
        return lhs.from == rhs.from && lhs.to == rhs.to && lhs.type == rhs.type
    }
    
}
