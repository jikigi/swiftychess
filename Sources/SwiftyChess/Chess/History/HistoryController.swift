//
//  HistoryController.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 12/29/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import Foundation

class HistoryController {
    
    private var moveAdapter: MoveAdapter = MoveAdapter()
    private var history: ChessHistoryCollection = ChessHistoryCollection<SingleChessMove>()
    
    func getMostCurrentMove() -> SingleChessMove? {
        return self.history.peek()
    }
    
    func getNextMove() -> SingleChessMove? {
        return self.history.getNextElement()
    }
    
    func canMoveBack() -> Bool {
        return self.history.peek() != nil 
    }
    
    func canMoveForward() -> Bool {
        return self.history.getNextElement() != nil
    }
    
    func moveBack() {
        self.history.decrementHead()
    }
    
    func getCurrentMoveNumber() -> Int {
        return self.history.getCurrentZeroBasedMoveNumber()
    }
    
    func getCurrentChessMoveNumber() -> Int {
        return self.history.getCurrentOneBasedMoveNumber()
    }
    
    func getCurrentIndividualMoveNumber() -> Int {
        return self.history.currentIndex
    }
    
    func getNumberOfIndividualMoves() -> Int {
        return self.history.index(after: self.history.currentIndex)
    }
    
    func getTotalNumberOfMoves() -> Int {
        return self.history.count
    }
    
    func reset() {
        self.history.moveToBeginning()
    }
    
    // if the previous move is a pawn that moved up two spaces, return the coordinate right behind the pawn
    // it is used to see if an enpassant is possible
    func getEnpassantMoveIfAvailable() -> Coordinate? {
        guard let lastMove = getMostCurrentMove(), lastMove.pieceInfo.type == .pawn else { return nil }
        guard abs(lastMove.to.rank - lastMove.from.rank) == 2 else { return nil }
        let enpassantRank = (lastMove.to.rank.rawValue + lastMove.from.rank.rawValue) / 2
        return Coordinate(file: lastMove.to.file, rank: Rank(rawValue: enpassantRank)!)
    }
    
    func createSingleMoveObject(piece: ChessPiece, from startCoord: Coordinate, to endCoord: Coordinate, takePiece: ChessPiece? = nil, takeCoord: Coordinate? = nil, promotion: PromotionSelection? = nil) -> SingleChessMove {
        var moveType: MoveType
        if let promotion = promotion {
            let takenPieceInfo = takePiece != nil ? self.moveAdapter.createPieceInfo(from: takePiece!) : nil
            moveType = .promotion(selection: promotion, color: piece.color, takenPiece: takenPieceInfo, takenCoord: takeCoord)
        } else if let takePiece = takePiece, let takeCoord = takeCoord {
            moveType = .take(piece: self.moveAdapter.createPieceInfo(from: takePiece), at: takeCoord)
        } else if piece is King, abs(startCoord.file - endCoord.file) == 2 {
            let castleDirection: CastleDirection
            if startCoord.file - endCoord.file > 0 {
                castleDirection = .queen
            } else {
                castleDirection = .king
            }
            moveType = .castle(direction: castleDirection)
        } else {
            moveType = .normal
        }
        
        var halfMoveNum: Int = 0
        if !(takePiece != nil && takeCoord != nil) && !(piece is Pawn), let lastMove = getMostCurrentMove() {
            halfMoveNum = lastMove.halfMoveNum + 1
        }
        
        return SingleChessMove(pieceInfo: self.moveAdapter.createPieceInfo(from: piece),
                               from: startCoord,
                               to: endCoord,
                               type: moveType,
                               halfMoveNum: halfMoveNum)
    }
    
    func getPromotionAndTakeInformation(for move: SingleChessMove) -> (ChessPiece?, Coordinate?, PromotionSelection?) {
        var takePiece: ChessPiece?
        var takeCoord: Coordinate?
        var promotion: PromotionSelection?
        if case let .take(pieceInfo, coordinate) = move.type {
            takePiece = self.moveAdapter.createPiece(from: pieceInfo)
            takeCoord = coordinate
        } else if case let .promotion(promotionSelection, _, pieceInfo, coordinate) = move.type {
            if let pieceInfo = pieceInfo {
                takePiece = self.moveAdapter.createPiece(from: pieceInfo)
            }
            takeCoord = coordinate
            promotion = promotionSelection
        }
        return (takePiece, takeCoord, promotion)
    }
    
    func record(move: SingleChessMove) {
        if let nextElement = self.history.getNextElement(), nextElement == move {
            self.history.incrementCurrentIndex()
            return
        }
        self.history.push(element: move)
    }
    
}
