//
//  HistoryPieceFactory.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 9/22/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation

public typealias PieceInfo = (type: PieceType, color: Color, movedNum: Int)

struct MoveAdapter {
    
    func createPiece(from pieceInfo: PieceInfo) -> ChessPiece {
        switch pieceInfo.type {
        case .pawn:
            return Pawn(color: pieceInfo.color, movedNum: pieceInfo.movedNum)
        case .knight:
            return Knight(color: pieceInfo.color, movedNum: pieceInfo.movedNum)
        case .bishop:
            return Bishop(color: pieceInfo.color, movedNum: pieceInfo.movedNum)
        case .rook:
            return Rook(color: pieceInfo.color, movedNum: pieceInfo.movedNum)
        case .queen:
            return Queen(color: pieceInfo.color, movedNum: pieceInfo.movedNum)
        case .king:
            return King(color: pieceInfo.color, movedNum: pieceInfo.movedNum)
        }
    }
    
    func createPieceInfo(from piece: ChessPiece) -> PieceInfo {
        var pieceType: PieceType
        if piece is Pawn {
            pieceType = .pawn
        } else if piece is Bishop {
            pieceType = .bishop
        } else if piece is Knight {
            pieceType = .knight
        } else if piece is Rook {
            pieceType = .rook
        } else if piece is Queen {
            pieceType = .queen
        } else {
            pieceType = .king
        }
        return (type: pieceType,
                color: piece.color,
                movedNum: piece.movedNum)
    }
}
