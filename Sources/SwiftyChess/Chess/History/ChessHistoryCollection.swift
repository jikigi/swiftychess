//
//  Queue.swift
//  Chess Openings
//
//  Created by Casey Evanish on 8/27/17.
//  Copyright © 2017 Casey Evanish. All rights reserved.
//

import Foundation

/// A collection type that keeps track of some form of chess history of something as well as keep track the current element.
// The history always begins at 0 and there is a pointer to keep track of the most recent element in the history
// when something is pushed onto this collection, it always pushes from the currentIndex and the most recent element is updated thereafter
// for example:
//
// [1, 2, 3, 4, 5, 6, 7]
//  current = 2
//  most recent = 7
//
// when a push happens, the resulting collection is
// [1, 2, 8, 3, 4, 5, 6, 7]
//  current = 8
//  most recent = 8
//  The elements 3 - 7 are now pretty much ignored. This is by design to enable fast reloading of games.
//

class ChessHistoryCollection<T: Equatable> {
    
    private var backingArray : [T]
    
    var currentIndex : Int
    private var head: Int
    
    init() {
        // starting at -1 because nothing has been pushed yet
        self.currentIndex = -1
        self.head = -1
        self.backingArray = []
    }
    
    // this gets the element at currentIndex
    func peek() -> T? {
        guard self.head >= self.backingArray.startIndex, self.backingArray.startIndex...self.head ~= self.currentIndex else { return nil }
        return self.backingArray[self.currentIndex]
    }
    
    func push(element: T) {
        incrementCurrentIndex()
        if self.currentIndex < self.backingArray.endIndex {
            self.backingArray[self.currentIndex] = element
        } else {
            self.backingArray.append(element)
        }
        self.head = self.currentIndex
    }
    
    func moveToBeginning() {
        self.currentIndex = -1
    }
    
    func getNextElement() -> T? {
        guard (self.currentIndex + 1) <= self.head else { return nil }
        return self.backingArray[self.currentIndex + 1]
    }
    
    func getPreviousElement() -> T? {
        guard (self.currentIndex - 1) >= self.backingArray.startIndex else { return nil }
        return self.backingArray[self.currentIndex - 1]
    }
    
    func incrementCurrentIndex() {
        self.currentIndex = Swift.min(self.currentIndex + 1, self.backingArray.endIndex)
    }
    
    func decrementHead() {
        self.currentIndex = Swift.max(self.currentIndex - 1, -1)
    }
    
    func getCurrentZeroBasedMoveNumber() -> Int {
        return (self.currentIndex + 2) / 2
    }
    
    func getCurrentOneBasedMoveNumber() -> Int {
        return (Swift.max(self.currentIndex, 0)/2) + 1
    }
}

extension ChessHistoryCollection: Collection {
    
    func index(after i: Int) -> Int {
        return i + 1
    }
    
    subscript(position: Int) -> T {
        return self.backingArray[position]
    }
    
    var startIndex: Int {
        return 0
    }
    
    var endIndex: Int {
        return index(after: self.head)
    }
    
}
