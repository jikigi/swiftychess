//
//  Board.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 9/16/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation

public struct Bitboard {
    static let aFile: UInt64 = 0x0101010101010101
    static let abFile: UInt64 = 0x0303030303030303
    static let hFile: UInt64 = 0x8080808080808080
    static let ghFile: UInt64 = 0xC0C0C0C0C0C0C0C0
    
    private var white: UInt64 = 0x000000000000FFFF
    private var black: UInt64 = 0xFFFF000000000000
    
    subscript(color: Color) -> UInt64 {
        get {
            return color == .black ? self.black : self.white
        }
        set(newValue) {
            switch color {
            case .black:
                self.black = newValue
            case .white:
                self.white = newValue
            }
        }
    }
    
    mutating func reset() {
        self.white = 0x000000000000FFFF
        self.black = 0xFFFF000000000000
    }
}

class Board {
    
    struct KingsInfo {
        private var white: (coordinate: Coordinate, king: King) = (Coordinate(file: .FILE_E, rank: .RANK_1), King(color: .white))
        private var black: (coordinate: Coordinate, king: King) = (Coordinate(file: .FILE_E, rank: .RANK_8), King(color: .black))
        
        subscript(color: Color) -> (coordinate: Coordinate, king: King) {
            get {
                return color == .black ? self.black : self.white
            }
            set(newValue) {
                switch color {
                case .black:
                    self.black = newValue
                case .white:
                    self.white = newValue
                }
            }
        }
        
        mutating func reset() {
            self.white = (Coordinate(file: .FILE_E, rank: .RANK_1), King(color: .white))
            self.black = (Coordinate(file: .FILE_E, rank: .RANK_8), King(color: .black))
        }
    }
    
    var pieces: [ChessPiece?] = []
    var kings: KingsInfo = KingsInfo()
    var bitboards: Bitboard = Bitboard()
    
    init() {
        initPieces()
    }
    
    func reset() {
        initPieces()
        self.kings.reset()
        self.bitboards.reset()
    }
    
    func getPiece(at coordinate: Coordinate) -> ChessPiece? {
        if self.pieces[coordinate.intVal] != nil {
            return self.pieces[coordinate.intVal]
        }
        for color in Color.allCases {
            if self.kings[color].coordinate == coordinate {
                return self.kings[color].king
            }
        }
        return nil
    }
    
    private func initPieces() {
        self.pieces = [ChessPiece?](repeating: nil, count: 64)
        
        //pawn
        for file in File.FILE_A...File.FILE_H {
            self.pieces[Coordinate(file: file , rank: .RANK_2).intVal] = Pawn(color: .white)
            self.pieces[Coordinate(file: file, rank: .RANK_7).intVal] = Pawn(color: .black)
        }
        
        //bishop
        self.pieces[Coordinate(file: .FILE_C, rank: .RANK_8).intVal] = Bishop(color: .black)
        self.pieces[Coordinate(file: .FILE_F, rank: .RANK_8).intVal] = Bishop(color: .black)
        self.pieces[Coordinate(file: .FILE_C, rank: .RANK_1).intVal] = Bishop(color: .white)
        self.pieces[Coordinate(file: .FILE_F, rank: .RANK_1).intVal] = Bishop(color: .white)
        
        //rook
        self.pieces[Coordinate(file: .FILE_A, rank: .RANK_8).intVal] = Rook(color: .black)
        self.pieces[Coordinate(file: .FILE_H, rank: .RANK_8).intVal] = Rook(color: .black)
        self.pieces[Coordinate(file: .FILE_A, rank: .RANK_1).intVal] = Rook(color: .white)
        self.pieces[Coordinate(file: .FILE_H, rank: .RANK_1).intVal] = Rook(color: .white)
        
        //Knight
        self.pieces[Coordinate(file: .FILE_B, rank: .RANK_8).intVal] = Knight(color: .black)
        self.pieces[Coordinate(file: .FILE_G, rank: .RANK_8).intVal] = Knight(color: .black)
        self.pieces[Coordinate(file: .FILE_B, rank: .RANK_1).intVal] = Knight(color: .white)
        self.pieces[Coordinate(file: .FILE_G, rank: .RANK_1).intVal] = Knight(color: .white)
        
        //Queen
        self.pieces[Coordinate(file: .FILE_D, rank: .RANK_8).intVal] = Queen(color: .black)
        self.pieces[Coordinate(file: .FILE_D, rank: .RANK_1).intVal] = Queen(color: .white)
    }
    
}
