//
//  OpeningBook.swift
//  Chess Openings
//
//  Created by Casey Evanish on 10/12/17.
//  Copyright © 2017 Casey Evanish. All rights reserved.
//

import Foundation

public struct MoveEntry: Equatable {
    var key: UInt64 = 0
    var move : UInt16 = 0
    var from: Coordinate!
    var to: Coordinate!
    var weight: UInt16 = 0
    var learn: UInt32 = 0
    
    public static func ==(lhs: MoveEntry, rhs: MoveEntry) -> Bool {
        return lhs.from == rhs.from && lhs.to == rhs.to
    }
}

class OpeningBook {
    
    var fileSize: Int
    
    var bookBytes = [UInt8]()
    
    var open: Bool
    
    init(filePath: String) {
        self.fileSize = 0
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: filePath)
            self.fileSize = attr[FileAttributeKey.size] as! Int
            
            if let data = NSData(contentsOfFile: filePath) {
                
                var buffer = [UInt8](repeating: 0, count: data.length)
                data.getBytes(&buffer, length: data.length)
                self.bookBytes = buffer
                
            }
            
        } catch {
            print("Error: \(error)")
        }
        
        self.open = true
    }
    
    convenience init() {
        self.init(filePath: Bundle.main.bundleURL.appendingPathComponent("ProDeo-2900.bin").path)
    }
    
    func close() {
        self.fileSize = 0
        self.bookBytes = []
        self.open = false
    }
    
    func getMoves(for key: UInt64) -> [MoveEntry]? {
        var index = search(for: key)
        guard index != -1 else {return nil}
        var entries: [MoveEntry] = []
        var dupIndex: Int?
        while(true) {
            guard let entry = getMoveEntry(startIndex: index) else {continue}
            guard entry.key == key else {break}
            guard entries.count < 100 else {return nil}
            dupIndex = entries.firstIndex(where: { possibleDup in
                possibleDup == entry
            })
            if let duplicate = dupIndex {
                entries[duplicate].weight += entry.weight
            } else {
                entries.append(entry)
            }
            index += 16
        }
        entries.sort(by: { first, second in
            first.weight > second.weight
        })
        return entries
    }
    
    private func search(for key: UInt64) -> Int {
        var start, middle, end : Int
        var k: UInt64
        
        start = 0
        end = self.fileSize/16
        
        while (true) {
            if(end-start==1){
                if let endKey = readInt(startIndex: end * 16), UInt64(endKey) == key {
                    return end * 16
                }
                return -1
            }
            middle = (start + end) / 2
            guard let midKey = readInt(startIndex: middle*16) else {return -1}
            k = UInt64(midKey)
            if (key <= k) {
                end = middle
            } else {
                start = middle
            }
        }
    }
    
    private func moveToCoord(move: UInt16) -> (Coordinate, Coordinate)? {
        let from = (Int(move >> 6))&0x077
        let from_rank = (from>>3)&0x7
        let from_file = from&0x7
        guard from_file < 8 && from_rank < 8 else {return nil}
        let fromCoord = Coordinate(file: File(rawValue: from_file + 1)!, rank: Rank(rawValue: from_rank + 1)!)
        let to = Int(move)&0x077
        let to_rank = (to>>3)&0x7
        let to_file = to&0x7
        guard to_file < 8 && to_rank < 8 else {return nil}
        let toCoord = Coordinate(file: File(rawValue: to_file + 1)!, rank: Rank(rawValue: to_rank + 1)!)
        return (fromCoord, toCoord)
    }
    
    private func getMoveEntry(startIndex: Int) -> MoveEntry? {
        var entry = MoveEntry()
        guard let key = readInt(startIndex: startIndex) else {return nil}
        entry.key = UInt64(key)
        guard let move = readInt(startIndex: startIndex+8, length: 2) else {return nil}
        entry.move = UInt16(move)
        guard let (from, to) = moveToCoord(move: entry.move) else {return nil}
        entry.from = from
        entry.to = to
        guard let weight = readInt(startIndex: startIndex+10, length: 2) else {return nil}
        entry.weight = UInt16(weight)
        guard let learn = readInt(startIndex: startIndex+12, length: 4) else {return nil}
        entry.learn = UInt32(learn)
        return entry
    }
    
    private func readInt(startIndex: Int, length: Int = 8) -> UInt? {
        var result : UInt = 0
        
        for index in 0..<length {
            guard startIndex+index<self.bookBytes.count else {return nil}
            result = (result << 8) + UInt(self.bookBytes[startIndex+index])
        }
        return result
    }
}
