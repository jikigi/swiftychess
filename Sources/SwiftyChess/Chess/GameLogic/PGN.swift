//
//  PGN.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 8/9/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation

public class PGN {
    
    public enum GameResult {
        case draw
        case white
        case black
    }
    
    public var event: String?
    public var site: String?
    public var date: String?
    public var round: Int?
    public var white: String?
    public var black: String?
    public var result: GameResult?
    public var whiteElo: Int?
    public var blackElo: Int?
    public var eco: String?
    
    public var notations: [String] = []
    
    public init?(url: URL) {
        guard let content = try? String(contentsOf: url) else { return nil }
        parsePGN(string: content)
    }
    
    public init(string: String) {
        parsePGN(string: string)
    }
    
    private func parsePGN(string: String) {
        
        let textRange = NSRange(location: 0, length: string.count)
        
        let regex = try! NSRegularExpression(pattern: "\\{\\[%clk\\s[0-9]+:[0-9]+:[0-9]+(\\.[0-9]+)?\\]\\}|([0-9]+\\.)(\\.\\.)?|\\[.*\\]|(0\\-1)|(1\\-0)|(1/2\\-1/2)|\\n", options: [])
        
        let replaced = regex.stringByReplacingMatches(in: string, options: [], range: textRange, withTemplate: " ")
        self.notations = replaced.split(separator: " ").map({String($0)})
        
        let tagPGN = try! NSRegularExpression(pattern: "\\[(?:Event \\\"(?<event>.*)\\\")\\]|\\[(?:Site \\\"(?<site>.*)\\\")\\]|\\[(?:Date \\\"(?<date>.*)\\\")\\]|\\[(?:Round \\\"(?<round>[\\d+|\\?])\\\")\\]|\\[(?:White \\\"(?<white>.*)\\\")\\]|\\[(?:Black \\\"(?<black>.*)\\\")\\]|(?:\\[Result \\\"(?<result>1\\/2\\-1\\/2|1-0|0-1)\\\"\\])|(?:\\[WhiteElo \\\"(?<whiteElo>\\d*)\\\"\\]?)|(?:\\[BlackElo \\\"(?<blackElo>\\d*)\\\"\\]?)|(?:\\[ECO \\\"(?<eco>[A-Z]\\d*)\\\"\\]?)", options: [])
        
        for match in tagPGN.matches(in: string, options: [], range: textRange) {
            if let eventRange = Range(match.range(withName: "event"), in: string) {
                self.event = String(string[eventRange])
            } else if let siteRange = Range(match.range(withName: "site"), in: string) {
                self.site = String(string[siteRange])
            } else if let dateRange = Range(match.range(withName: "date"), in: string) {
                self.date = String(string[dateRange])
            } else if let roundRange = Range(match.range(withName: "round"), in: string) {
                self.round = Int(string[roundRange])
            } else if let whiteRange = Range(match.range(withName: "white"), in: string) {
                self.white = String(string[whiteRange])
            } else if let blackRange = Range(match.range(withName: "black"), in: string) {
                self.black = String(string[blackRange])
            } else if let resultRange = Range(match.range(withName: "result"), in: string) {
                let resultString = String(string[resultRange])
                var result: GameResult?
                switch resultString {
                case "1/2-1/2":
                    result = .draw
                case "1-0":
                    result = .white
                case "0-1":
                    result = .black
                case "*":
                    result = nil
                default:
                    result = nil
                }
                self.result = result
            } else if let whiteEloRange = Range(match.range(withName: "whiteElo"), in: string) {
                self.whiteElo = Int(string[whiteEloRange])
            } else if let blackEloRange = Range(match.range(withName: "blackElo"), in: string) {
                self.blackElo = Int(string[blackEloRange])
            } else if let ecoRange = Range(match.range(withName: "eco"), in: string) {
                self.eco = String(string[ecoRange])
            }
        }
    }
}
