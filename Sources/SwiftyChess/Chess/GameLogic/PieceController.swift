//
//  PieceController.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 12/29/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import Foundation

struct Checked {
    var checkingPiece: ChessPiece
    var checkingCoordinate: Coordinate
    
    var doubleCheck: Bool = false
}

class PieceController {
    
    private(set) var board: Board = Board()
    var pieces: [ChessPiece?] {
        var pieceMapCopy = self.board.pieces
        let (whiteKingCoord, whiteKing) = self.board.kings[.white]
        let (blackKingCoord, blackKing) = self.board.kings[.black]
        pieceMapCopy[whiteKingCoord.intVal] = whiteKing
        pieceMapCopy[blackKingCoord.intVal] = blackKing
        return pieceMapCopy
    }
    
    private(set) var castleRights: Castling = Castling()
    private(set) var check: Checked? = nil
    
    private var moveCache: [UInt64] = [UInt64](repeating: UInt64.max, count: 64)
    
    func reset() {
        self.moveCache = [UInt64](repeating: UInt64.max, count: 64)
        self.check = nil
        self.castleRights = Castling()
        self.board.reset()
    }
    
    func isCheckmate(enpassantCoord: Coordinate?) -> Bool {
        guard let checked = self.check else { return false }
        let otherColor = !checked.checkingPiece.color
        let allMoves = self.pieces.enumerated().reduce(into: UInt64(0), { allMoves, element in
            let (offset, piece) = element
            guard let nonNullPiece = piece, nonNullPiece.color == otherColor else { return }
            allMoves |= self.legalMoves(forPieceAt: offset.toChessCoord(), enpassantCoord: enpassantCoord)
        })
        return (legalMoves(forPieceAt: self.board.kings[!checked.checkingPiece.color].coordinate,
                           enpassantCoord: enpassantCoord) | allMoves) == 0
    }
    
    func move(from coord1: Coordinate,
              to coord2: Coordinate,
              enpassantCoord: Coordinate?,
              promotion: PromotionSelection?) {
        guard let piece = self.board.getPiece(at: coord1) else { return }
        self.check = nil
        self.moveCache = [UInt64](repeating: UInt64.max, count: 64)
        if piece.hasntMoved {
            if piece is King {
                self.castleRights[piece.color, .king] = false
                self.castleRights[piece.color, .queen] = false
            } else if piece is Rook {
                if coord1.file == .FILE_A {
                    self.castleRights[piece.color, .queen] = false
                } else if coord1.file == .FILE_H {
                    self.castleRights[piece.color, .king] = false
                }
            }
        }
        move(piece: piece,
             from: coord1,
             to: coord2,
             enpassantCoord: enpassantCoord,
             promotion: promotion)
        
        // redefine piece in case of promotion
        guard let newPiece = self.board.getPiece(at: coord2) else { return }
        checkPinned(enpassantCoord: enpassantCoord)
        checkForCheck(pieceThatJustMoved: newPiece,
                      movedTo: coord2,
                      enpassantCoord: enpassantCoord)
    }
    
    func moveBack(from coord1: Coordinate,
                  to coord2: Coordinate,
                  takePiece: ChessPiece?,
                  takeCoord: Coordinate?,
                  enpassantCoord: Coordinate?,
                  promotion: PromotionSelection?,
                  moveBeforeLastMovedTo lastMoveCoord: Coordinate?) {
        guard let piece = self.board.getPiece(at: coord1) else { return }
        self.check = nil
        self.moveCache = [UInt64](repeating: UInt64.max, count: 64)
        moveBack(piece: piece,
                 from: coord1,
                 to: coord2,
                 takePiece: takePiece,
                 takeCoord: takeCoord,
                 promotion: promotion)
        
        // redefine piece in case of promotion
        guard let newPiece = self.board.getPiece(at: coord2) else { return }
        
        if newPiece.hasntMoved, newPiece is King || newPiece is Rook {
            if let king = newPiece as? King {
                // queenside
                let rook1 = self.board.getPiece(at: Coordinate(file: .FILE_A, rank: coord2.rank)) as? Rook
                self.castleRights[king.color, .queen] = rook1 != nil && rook1!.hasntMoved
                // kingside
                let rook2 = self.board.getPiece(at: Coordinate(file: .FILE_H, rank: coord2.rank)) as? Rook
                self.castleRights[king.color, .king] = rook2 != nil && rook2!.hasntMoved
            } else if let rook = newPiece as? Rook {
                let castleDirec: CastleDirection = coord2.file == .FILE_A ? .queen : .king
                let king = self.board.getPiece(at: Coordinate(file: .FILE_E, rank: coord2.rank)) as? King
                self.castleRights[rook.color, castleDirec] = king != nil && king!.hasntMoved
            }
        }
        
        checkPinned(enpassantCoord: enpassantCoord)
        checkForCheck(pieceThatJustMoved: newPiece, movedTo: coord2, enpassantCoord: enpassantCoord)
        
        guard let lastMoveCoord = lastMoveCoord,
            let lastMovedPiece = self.board.getPiece(at: lastMoveCoord),
            lastMovedPiece.color != newPiece.color else { return }
        checkForCheck(pieceThatJustMoved: lastMovedPiece, movedTo: lastMoveCoord, enpassantCoord: enpassantCoord)
    }
    
    func legalMoves(forPieceAt coordinate: Coordinate, enpassantCoord: Coordinate?) -> UInt64 {
        let coordIntVal = coordinate.intVal
        guard let piece = self.board.getPiece(at: coordinate) else { return 0 }
        var legalMoves = getLegalMoves(for: piece, at: coordinate, enpassantCoord: enpassantCoord)
        if let checked = self.check {
            if !(piece is King) {
                if checked.doubleCheck {
                    // there is a double check and the king has to move
                    // make it impossible for the piece to move
                    
                    //TODO: Test case - test case where enpassant leads to a double check
                    //https://en.wikipedia.org/wiki/Double_check
                    legalMoves = 0
                } else {
                    // single check
                    var firstPieceMoves: UInt64 = 0
                    if let rangePiece = checked.checkingPiece as? SlidingPiece {
                        // if the piece is a range piece, just get the moves that attack the king
                        let (kingCoord, _) = self.board.kings[!checked.checkingPiece.color]
                        guard let possibleRange = rangePiece.getPossiblePinRange(from: checked.checkingCoordinate, to: kingCoord) else { return legalMoves }
                        firstPieceMoves = possibleRange
                    }
                    let moveAttackingPiece = piece.movesAttacking(piece: checked.checkingPiece, at: checked.checkingCoordinate, given: legalMoves, enpassantCoord: enpassantCoord)
                    firstPieceMoves.add(moveAttackingPiece)
                    
                    legalMoves &= firstPieceMoves
                }
            } else {
                // kind of a hack
                legalMoves &= piece.allAttackMoves(from: coordinate, enpassantCoord: enpassantCoord)
            }
        }
        self.moveCache[coordIntVal] = legalMoves
        return legalMoves
    }
    
}

// MARK: private methods
extension PieceController {
    
    private func checkForCheck(pieceThatJustMoved: ChessPiece,
                               movedTo moveCoordinate: Coordinate,
                               enpassantCoord: Coordinate?) {
        let (kingCoord, king) = self.board.kings[!pieceThatJustMoved.color]
        var checked: Checked? = nil
        // checks if the piece that just moved checks the other king
        if getLegalMoves(for: pieceThatJustMoved, at: moveCoordinate, enpassantCoord: enpassantCoord).contains(kingCoord.uint64Val) {
            checked = Checked(checkingPiece: pieceThatJustMoved, checkingCoordinate: moveCoordinate, doubleCheck: false)
        }
        // for discovery checks
        for case let (index, piece as (SlidingPiece & ChessPiece)) in self.board.pieces.enumerated() where piece.color != king.color {
            let coordinate = index.toChessCoord()
            guard coordinate != moveCoordinate,
                  self.legalMoves(forPieceAt: coordinate, enpassantCoord: enpassantCoord).contains(kingCoord.uint64Val) else { continue }
            if checked?.checkingPiece == nil {
                checked = Checked(checkingPiece: piece, checkingCoordinate: coordinate, doubleCheck: false)
            } else {
                checked?.doubleCheck = true
            }
        }
        self.check = checked
    }
    
    private func checkPinned(enpassantCoord: Coordinate?) {
        let bitboards = self.board.bitboards
        for case let (index, piece as (SlidingPiece & ChessPiece)) in self.board.pieces.enumerated()
            where piece.inSight(of: self.board.kings[!piece.color].coordinate, from: index.toChessCoord()) {
                let coord = index.toChessCoord()
                // make sure that there is some range from the piece to the king
                // if this piece is in sight, this should technically never be nil
                guard var pinnableRange = piece.getPossiblePinRange(from: coord, to:
                    self.board.kings[!piece.color].coordinate) else { continue }

                // get the pieces with the opposite color within the pin range
                let movesWithOtherColor = pinnableRange & bitboards[!piece.color]
                // get the pieces with the same color within the pin range
                let movesWithThisColor = pinnableRange & (bitboards[piece.color] |
                    self.board.kings[piece.color].coordinate.uint64Val)
                // make sure that there are no pieces with the same color in the pin range
                // and only one piece of the opposite color in the pin range
                guard movesWithOtherColor.hasOnlyOneBitSet && movesWithThisColor == 0 else { continue }
                // technically since there is only one bit set, we know the coordinate of the pinned piece
                let pinnedPieceCoord = movesWithOtherColor.toCoord()
                // make sure the piece exists
                guard let pinnedPiece = self.board.getPiece(at: pinnedPieceCoord) else { continue }

                // add the position of the pinning piece as a possible move for the pinned piece
                pinnableRange.add(coord.uint64Val)

                let pinnedPieceLegalMoves = getLegalMoves(for: pinnedPiece,
                                                          at: pinnedPieceCoord,
                                                          enpassantCoord: enpassantCoord)
                self.moveCache[pinnedPieceCoord.intVal] = pinnedPieceLegalMoves & pinnableRange
        }
    }
    
    private func getLegalMoves(for piece: ChessPiece,
                               at coordinate: Coordinate,
                               enpassantCoord: Coordinate?) -> UInt64 {
        let coordIntVal = coordinate.intVal
        guard self.moveCache[coordIntVal] == UInt64.max else {
            return self.moveCache[coordIntVal]
        }
        var pieceMapCopy = self.board.pieces
        let (kingCoord, king) = self.board.kings[piece.color]
        // we don't include the other king for range pieces so
        // that we can calculate possible king moves better (with skewing and all)
        if !(piece is SlidingPiece) {
            let (otherKingCoord, otherKing) = self.board.kings[!piece.color]
            pieceMapCopy[otherKingCoord.intVal] = otherKing
        }
        pieceMapCopy[kingCoord.intVal] = king
        return piece.legalMoves(at: coordinate,
                                enpassantCoord: enpassantCoord,
                                pieceMap: pieceMapCopy,
                                bitboards: self.board.bitboards,
                                castleRights: self.castleRights)
    }
    
    func move(piece: ChessPiece,
              from coord1: Coordinate,
              to coord2: Coordinate,
              enpassantCoord: Coordinate?,
              promotion: PromotionSelection?) {
        var piece = piece
        var thisColorBitboard = self.board.bitboards[piece.color]
        var otherColorBitboard = self.board.bitboards[!piece.color]
        if let king = piece as? King {
            self.board.kings[king.color].king.movedNum += 1
            self.board.kings[king.color].coordinate = coord2
            thisColorBitboard.remove(coord1.uint64Val)
            thisColorBitboard.add(coord2.uint64Val)
            
            // if king takes a piece
            if self.board.pieces[coord2.intVal] != nil {
                self.board.pieces[coord2.intVal] = nil
                otherColorBitboard.remove(coord2.uint64Val)
            }
            
            // castling
            let fileDifference = coord2.file - coord1.file
            if abs(fileDifference) == 2 {
                // queenside castle
                var rookCoord: Coordinate = Coordinate(file: .FILE_A, rank: coord2.rank)
                var nextRookCoord: Coordinate = Coordinate(file: .FILE_D, rank: coord2.rank)
                if fileDifference > 0 {
                    // kingside castle
                    rookCoord = Coordinate(file: .FILE_H, rank: coord2.rank)
                    nextRookCoord = Coordinate(file: .FILE_F, rank: coord2.rank)
                }
                // should never be nil
                if var rook = self.pieces[rookCoord.intVal] {
                    rook.movedNum += 1
                    self.board.pieces[nextRookCoord.intVal] = rook
                    self.board.pieces[rookCoord.intVal] = nil
                    thisColorBitboard.add(nextRookCoord.uint64Val)
                    thisColorBitboard.remove(rookCoord.uint64Val)
                }
            }
        } else {
            if let enpassantCoord = enpassantCoord, coord2 == enpassantCoord, piece is Pawn {
                let otherPawnRank: Rank? = piece.color == .black ? coord2.rank + 1 : coord2.rank - 1
                let otherPawnCoord = Coordinate(file: coord2.file, rank: otherPawnRank!)
                self.board.pieces[otherPawnCoord.intVal] = nil
                otherColorBitboard.remove(otherPawnCoord.uint64Val)
            }
            self.board.pieces[coord1.intVal] = nil
            thisColorBitboard.remove(coord1.uint64Val)
            // transfer movedNum over
            let oldMoveNum = piece.movedNum
            if let promotion = promotion {
                switch promotion {
                case .rook:
                    piece = Rook(color: piece.color)
                case .bishop:
                    piece = Bishop(color: piece.color)
                case .queen:
                    piece = Queen(color: piece.color)
                case .knight:
                    piece = Knight(color: piece.color)
                }
            }
            piece.movedNum = oldMoveNum + 1
            self.board.pieces[coord2.intVal] = piece
            thisColorBitboard.add(coord2.uint64Val)
            otherColorBitboard.remove(coord2.uint64Val)
        }
        self.board.bitboards[piece.color] = thisColorBitboard
        self.board.bitboards[!piece.color] = otherColorBitboard
    }
    
    func moveBack(piece: ChessPiece,
                  from coord1: Coordinate,
                  to coord2: Coordinate,
                  takePiece: ChessPiece?,
                  takeCoord: Coordinate?,
                  promotion: PromotionSelection?) {
        var piece = piece
        var thisColorBitboard = self.board.bitboards[piece.color]
        var otherColorBitboard = self.board.bitboards[!piece.color]
        if let king = piece as? King {
            self.board.kings[king.color].king.movedNum -= 1
            self.board.kings[king.color].coordinate = coord2
            thisColorBitboard.remove(coord1.uint64Val)
            thisColorBitboard.add(coord2.uint64Val)
            
            //castling
            let fileDifference = coord2.file - coord1.file
            if abs(fileDifference) == 2 {
                // queenside castle
                var rookCoord: Coordinate = Coordinate(file: .FILE_D, rank: coord2.rank)
                var nextRookCoord: Coordinate = Coordinate(file: .FILE_A, rank: coord2.rank)
                if fileDifference < 0 {
                    // kingside castle
                    rookCoord = Coordinate(file: .FILE_F, rank: coord2.rank)
                    nextRookCoord = Coordinate(file: .FILE_H, rank: coord2.rank)
                }
                // should never be nil
                if var rook = self.pieces[rookCoord.intVal] {
                    rook.movedNum -= 1
                    self.board.pieces[nextRookCoord.intVal] = rook
                    self.board.pieces[rookCoord.intVal] = nil
                    thisColorBitboard.add(nextRookCoord.uint64Val)
                    thisColorBitboard.remove(rookCoord.uint64Val)
                }
            }
        } else {
            self.board.pieces[coord1.intVal] = nil
            thisColorBitboard.remove(coord1.uint64Val)
            if promotion != nil {
                var pawnPiece = Pawn(color: piece.color)
                pawnPiece.movedNum = piece.movedNum
                piece = pawnPiece
            }
            piece.movedNum -= 1
            self.board.pieces[coord2.intVal] = piece
            thisColorBitboard.add(coord2.uint64Val)
        }
        if let takePiece = takePiece, let takeCoord = takeCoord {
            self.board.pieces[takeCoord.intVal] = takePiece
            otherColorBitboard.add(takeCoord.uint64Val)
        }
        self.board.bitboards[piece.color] = thisColorBitboard
        self.board.bitboards[!piece.color] = otherColorBitboard
    }
    
}
