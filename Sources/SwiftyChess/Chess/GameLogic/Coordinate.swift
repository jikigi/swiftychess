//
//  Coordinate.swift
//  Chess Openings
//
//  Created by Casey Evanish on 9/5/17.
//  Copyright © 2017 Casey Evanish. All rights reserved.
//

import Foundation

public enum File : Int, Strideable, Hashable, Equatable, Comparable, CaseIterable {
    case FILE_A = 1, FILE_B = 2, FILE_C = 3, FILE_D = 4, FILE_E = 5, FILE_F = 6, FILE_G = 7, FILE_H = 8
    case TOP_END = 9
    
    public var bitOffset: Int {
        return self.rawValue - 1
    }
    
    public var letterRepresentation: String {
        switch self {
        case .FILE_A:
            return "a"
        case .FILE_B:
            return "b"
        case .FILE_C:
            return "c"
        case .FILE_D:
            return "d"
        case .FILE_E:
            return "e"
        case .FILE_F:
            return "f"
        case .FILE_G:
            return "g"
        case .FILE_H:
            return "h"
        default:
            return ""
        }
    }
    
    public var hashValue: Int {
        return self.rawValue
    }
    
    public var mirrored: File {
        return File(rawValue: 8 - (self.rawValue - 1))!
    }
    
    // Operator Overrides
    public static func ==(lhs: File, rhs: File) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
    
    public static func >(lhs: File, rhs: File) -> Bool {
        return lhs.rawValue > rhs.rawValue
    }
    
    public static func <(lhs: File, rhs: File) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
    
    public static func >=(lhs: File, rhs: File) -> Bool {
        return lhs.rawValue >= rhs.rawValue
    }
    
    public static func <=(lhs: File, rhs: File) -> Bool {
        return lhs.rawValue <= rhs.rawValue
    }
    
    public static func + (left: File, right: Int) -> File? {
        let sum = (UInt8(1) << left.bitOffset) << right
        return sum != 0 ? File(rawValue: Int(log2(Double(sum))) + 1)! : nil
    }
    
    public static func - (left: File, right: Int) -> File? {
        let sum = (UInt8(1) << left.bitOffset) >> right
        return sum != 0 ? File(rawValue: Int(log2(Double(sum))) + 1)! : nil
    }
    
    public static func - (left: File, right: File) -> Int {
        return left.rawValue - right.rawValue
    }
    
    // Strideable Conformance
    public func distance(to other: File) -> File.Stride {
        return Stride(other.rawValue) - Stride(self.rawValue)
    }
    
    public func advanced(by n: File.Stride) -> File {
        return File(rawValue: numericCast(Stride(self.rawValue) + n))!
    }
    
    public typealias Stride = Int
    
    // CaseIterable Conformance
    public typealias AllCases = [File]
    public static var allCases: File.AllCases {
        get {
            return [.FILE_A, .FILE_B, .FILE_C, .FILE_D, .FILE_E, .FILE_F, .FILE_G, .FILE_H]
        }
    }
}

public enum Rank : Int, Strideable, Hashable, Equatable, Comparable, CaseIterable {
    case RANK_1 = 1, RANK_2 = 2, RANK_3 = 3, RANK_4 = 4, RANK_5 = 5, RANK_6 = 6, RANK_7 = 7, RANK_8 = 8
    case TOP_END = 9
    
    public var bitOffset: Int {
        return self.rawValue - 1
    }
    
    public var hashValue: Int {
        return self.rawValue
    }
    
    public var mirrored: Rank {
        return Rank(rawValue: 8 - (self.rawValue - 1))!
    }
    
    // Operator Overrides
    public static func ==(lhs: Rank, rhs: Rank) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
    
    public static func >(lhs: Rank, rhs: Rank) -> Bool {
        return lhs.rawValue > rhs.rawValue
    }
    
    public static func <(lhs: Rank, rhs: Rank) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
    
    public static func >=(lhs: Rank, rhs: Rank) -> Bool {
        return lhs.rawValue >= rhs.rawValue
    }
    
    public static func <=(lhs: Rank, rhs: Rank) -> Bool {
        return lhs.rawValue <= rhs.rawValue
    }
    
    public static func + (left: Rank, right: Int) -> Rank? {
        let sum = (UInt8(1) << left.bitOffset) << right
        return sum != 0 ? Rank(rawValue: Int(log2(Double(sum))) + 1)! : nil
    }
    
    public static func - (left: Rank, right: Int) -> Rank? {
        let sum = (UInt8(1) << left.bitOffset) >> right
        return sum != 0 ? Rank(rawValue: Int(log2(Double(sum))) + 1)! : nil
    }
    
    public static func - (left: Rank, right: Rank) -> Int {
        return left.rawValue - right.rawValue
    }
    
    // Strideable Conformance
    public func distance(to other: Rank) -> Rank.Stride {
        return Stride(other.rawValue) - Stride(self.rawValue)
    }
    
    public func advanced(by n: Rank.Stride) -> Rank {
        return Rank(rawValue: numericCast(Stride(self.rawValue) + n))!
    }
    
    public typealias Stride = Int
    
    // CaseIterable Conformance
    public typealias AllCases = [Rank]
    public static var allCases: Rank.AllCases {
        get {
            return [.RANK_1, .RANK_2, .RANK_3, .RANK_4, .RANK_5, .RANK_6, .RANK_7, .RANK_8]
        }
    }
}

public enum Coordinate: Int, CaseIterable, Hashable {
    
    case a1, b1, c1, d1, e1, f1, g1, h1,
    a2, b2, c2, d2, e2, f2, g2, h2,
    a3, b3, c3, d3, e3, f3, g3, h3,
    a4, b4, c4, d4, e4, f4, g4, h4,
    a5, b5, c5, d5, e5, f5, g5, h5,
    a6, b6, c6, d6, e6, f6, g6, h6,
    a7, b7, c7, d7, e7, f7, g7, h7,
    a8, b8, c8, d8, e8, f8, g8, h8
    
    public var file: File {
        return File(rawValue: (self.rawValue % 8) + 1)!
    }
    
    public var rank: Rank {
        return Rank(rawValue: (self.rawValue / 8) + 1)!
    }
    
    public var uint64Val: UInt64 {
        return 1 << self.rawValue
    }
    
    public var intVal: Int {
        return self.rawValue
    }
    
    public init?(file: File?, rank: Rank?) {
        guard let file = file, let rank = rank else { return nil }
        self.init(file: file, rank: rank)
    }
    
    public init(file: File, rank: Rank) {
        self.init(rawValue: ((rank.rawValue - 1) * 8) + (file.rawValue - 1))!
    }
    
    public func toNotation() -> String {
        var notation = ""
        notation += Constants.num2LetterDict[self.file.rawValue]!
        notation += String(self.rank.rawValue)
        return notation
    }
    
}
