//
//  FEN.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 2/10/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation

public class FEN {
    
    public var string: String = Constants.startingFEN
    
    func update(turnNum: Int, move: SingleChessMove, piece: ChessPiece, castleRights: Castling, pieces: [ChessPiece?]) {
        
        let start = move.from
        let end = move.to
        // positions string
        var newFen = updatedPieceString(from: move.from, to: move.to, pieces: pieces)
        
        // next turn
        newFen += " \(nextTurnString(currentColor: piece.color))"
        
        // castling
        newFen += " \(castleString(castleRights: castleRights))"
        
        // en passant
        newFen += " "
        
        if piece is Pawn, abs(end.rank - start.rank) == 2 {
            let halfwayRank = Rank(rawValue: (end.rank.rawValue + start.rank.rawValue) / 2 )!
            newFen += Coordinate(file: end.file, rank: halfwayRank).toNotation()
        } else {
            newFen += "-"
        }
        
        //half move count
        newFen += " \(move.halfMoveNum)"

        newFen += " \(turnNum)"
        self.string = newFen
    }
    
    func revert(from start: Coordinate, to end: Coordinate, turnNum: Int, previousMove: SingleChessMove?, pieceInfo: PieceInfo, castleRights: Castling, pieces: [ChessPiece?]) {
        // positions string
        var newFen = updatedPieceString(from: start, to: end, pieces: pieces)
        
        // next turn
        newFen += " \(nextTurnString(currentColor: !pieceInfo.color))"
        
        // castling
        newFen += " \(castleString(castleRights: castleRights))"
        
        // en passant
        newFen += " "
        if let prevMove = previousMove {
            if prevMove.pieceInfo.type == .pawn, abs(prevMove.to.rank - prevMove.from.rank) == 2 {
                let currRank: Rank = prevMove.pieceInfo.color == .black ? .RANK_6 : .RANK_3
                newFen += Coordinate(file: prevMove.to.file, rank: currRank).toNotation()
            } else {
                newFen += "-"
            }
        } else {
            newFen += "-"
        }
        
        // half move count
        newFen += " \(previousMove?.halfMoveNum ?? 0)"
        
        // turn number
        newFen += " \(turnNum)"
        self.string = newFen
    }
    
    private func updateRow(rank: Rank, pieces: [ChessPiece?]) -> String {
        return (File.FILE_A...File.FILE_H).reduce(into: "", { fen, file in
            if let piece = pieces[Coordinate(file: file, rank: rank).intVal] {
                fen += FEN.representation(for: piece)
            } else if let lastChar = fen.last,
                let lastInt = Int(String(lastChar)) {
                let indexBeforeLast = fen.index(before: fen.endIndex)
                fen = fen.replacingCharacters(in: indexBeforeLast..., with: String(lastInt + 1))
            } else {
                fen += "1"
            }
        })
    }
    
    private func updatedPieceString(from start: Coordinate, to end: Coordinate, pieces: [ChessPiece?]) -> String {
        var newFen = ""
        
        let spaces = self.string.components(separatedBy: " ")
        
        //piece placement
        var rows = Array(spaces[0].components(separatedBy: "/").reversed())
        rows[start.rank.bitOffset] = updateRow(rank: start.rank, pieces: pieces)
        rows[end.rank.bitOffset] = updateRow(rank: end.rank, pieces: pieces)
        newFen += rows.reversed().joined(separator: "/")
        
        return newFen
    }
    
    private func nextTurnString(currentColor color: Color) -> String {
        return "\((!color).toString())"
    }
    
    private func castleString(castleRights: Castling) -> String {
        var newFen = ""
        var castlePossible = false
        
        if castleRights[.white, .king] {
            newFen += "K"
            castlePossible = true
        }
        if castleRights[.white, .queen] {
            newFen += "Q"
            castlePossible = true
        }
        if castleRights[.black, .king] {
            newFen += "k"
            castlePossible = true
        }
        if castleRights[.black, .queen] {
            newFen += "q"
            castlePossible = true
        }
        if !castlePossible {
            newFen += "-"
        }
        return newFen
    }
    
}

extension FEN {
    
    public static func representation(for piece: ChessPiece) -> String {
        if piece is Pawn {
            return piece.color == .black ? "p" : "P"
        } else if piece is Bishop {
            return piece.color == .black ? "b" : "B"
        } else if piece is Knight {
            return piece.color == .black ? "n" : "N"
        } else if piece is Rook {
            return piece.color == .black ? "r" : "R"
        } else if piece is Queen {
            return piece.color == .black ? "q" : "Q"
        } else {
            return piece.color == .black ? "k" : "K"
        }
    }
    
}

////MARK: Retrieving an array of images that represents what a board should look like
//extension FEN {
//
//    func imageDict(from fenString: String) -> [UIImage?] {
//        guard let positionStrings = fenString.components(separatedBy: .whitespaces).first else { return [] }
//        var imageArray: [UIImage?] = Array(repeating: nil, count: 64)
//        var offset: Int = 0
//        for string in positionStrings.components(separatedBy: "/").reversed() {
//            for character in string {
//                if let number = Int(String(character)) {
//                    offset += number
//                } else {
//                    imageArray[offset] = UIImage(fenLetter: character)
//                    offset += 1
//                }
//            }
//        }
//        return imageArray
//    }
//
//}
