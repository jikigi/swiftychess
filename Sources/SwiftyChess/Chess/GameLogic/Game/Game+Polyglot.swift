//
//  Game+Polyglot.swift
//  SwiftyChess
//
//  Created by Casey Evanish on 7/14/20.
//

import Foundation

extension Game {
    
    public func getCurrentPGKey() -> UInt64 {
        precondition(self.configuration.contains(.polyglot), "The configuration must contain polyglot to obtain the polyglot key")
        return self.polyglot.key
    }
    
    public func getCurrentZobrist() -> UInt64 {
        precondition(self.configuration.contains(.polyglot), "The configuration must contain polyglot to obtain the zobrist hash")
        return self.polyglot.zobrist
    }
    
}
