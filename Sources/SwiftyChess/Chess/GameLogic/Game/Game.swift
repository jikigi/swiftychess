//
//  Game.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 12/29/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import Foundation

public class Game {
    
    public enum Configuration {
        case san
        case fen
        case polyglot
    }
    
    public var turn: Color = .white
    
    var polyglot: PGKey = PGKey()
    var fen: FEN = FEN()
    var san: SAN = SAN()
    
    private var pieceController = PieceController()
    private var historyController = HistoryController()
    
    var castleRights: Castling {
        return self.pieceController.castleRights
    }
    
    public var isInCheck: Bool {
        return self.pieceController.check != nil
    }
    
    public var pieces: [ChessPiece?] {
        return self.pieceController.pieces
    }
    
    var configuration: Set<Configuration>
    
    public init(configuration: Set<Configuration> = [.fen, .polyglot, .san]) {
        self.configuration = configuration
    }
    
    public func piece(at coordinate: Coordinate) -> ChessPiece? {
        return self.pieceController.board.getPiece(at: coordinate)
    }
    
    public func reset() {
        self.pieceController.reset()
        self.historyController.reset()
        if self.configuration.contains(.san) {
            self.san.reset()
        }
        if self.configuration.contains(.polyglot) {
            self.polyglot = PGKey()
        }
        if self.configuration.contains(.fen) {
            self.fen = FEN()
        }
        self.turn = .white
    }
    
    public func getLastMove() -> SingleChessMove? {
        return self.historyController.getMostCurrentMove()
    }
    
    public func getNextMove() -> SingleChessMove? {
        return self.historyController.getNextMove()
    }
    
    public func legalMoves(forPieceAt coordinate: Coordinate) -> UInt64 {
        return self.pieceController.legalMoves(forPieceAt: coordinate, enpassantCoord: self.historyController.getEnpassantMoveIfAvailable())
    }
    
    public func legalCoordinates(forPieceAt coordinate: Coordinate) -> [Coordinate] {
        return legalMoves(forPieceAt: coordinate).toCoordinateArray()
    }
    
    /// returns the chess move number that the game is currently at
    public func getCurrentChessMoveNumber() -> Int {
        return self.historyController.getCurrentChessMoveNumber()
    }
    
    public func getCurrentIndividualMoveNumber() -> Int {
        return self.historyController.getCurrentIndividualMoveNumber()
    }
    
    public func getTotalNumberOfIndividualMoves() -> Int {
        return self.historyController.getTotalNumberOfMoves()
    }
    
}

extension Game {
    
    /// returns the number of moves that have been played
    public func getPlayedIndividualMovesCount() -> Int {
        return self.historyController.getNumberOfIndividualMoves()
    }
    
}

// going back and forth
extension Game {
    
    public func canGoBack() -> Bool {
        return self.historyController.canMoveBack()
    }
    
    public func canGoForward() -> Bool {
        return self.historyController.canMoveForward()
    }
    
    private func getChessMove(from coord1: Coordinate, to coord2: Coordinate, promotion: PromotionSelection? = nil) -> SingleChessMove? {
        guard let piece = piece(at: coord1) else { return nil }
        let enpassantCoord = self.historyController.getEnpassantMoveIfAvailable()
        var takePiece: ChessPiece? = self.piece(at: coord2)
        var takeCoord: Coordinate? = nil
        if takePiece != nil {
            takeCoord = coord2
        }
        if piece is Pawn && coord2 == enpassantCoord {
            let enpassantOffset = piece.color == .white ? -1 : 1
            if let enpassantTakeCoord = Coordinate(file: coord2.file, rank: coord2.rank + enpassantOffset) {
                takePiece = self.piece(at: enpassantTakeCoord)
                takeCoord = enpassantTakeCoord
            }
        }
        return self.historyController.createSingleMoveObject(piece: piece, from: coord1, to: coord2, takePiece: takePiece, takeCoord: takeCoord, promotion: promotion)
    }
    
    @discardableResult
    public func move(from coord1: Coordinate, to coord2: Coordinate, promotion: PromotionSelection? = nil) -> SingleChessMove? {
        guard let move = getChessMove(from: coord1, to: coord2, promotion: promotion), let piece = piece(at: coord1) else { return nil }
        
        // get the same piece that could go to the same coordinate
        // get this before the piece actually moves
        let conflicting: Coordinate? = self.pieceController.pieces.enumerated().first(where: { (index, otherPiece) in
            guard let nonNullPiece = otherPiece,
                index.toChessCoord() != coord1,
                nonNullPiece.color == piece.color,
                type(of: nonNullPiece) == type(of: piece) else { return false }
            return legalMoves(forPieceAt: index.toChessCoord()).contains(coord2.uint64Val)
        })?.offset.toChessCoord()
        
        self.pieceController.move(
            from: coord1,
            to: coord2,
            enpassantCoord: self.historyController.getEnpassantMoveIfAvailable(),
            promotion: promotion
        )
        
        self.historyController.record(move: move)
        
        if let lastMove = self.historyController.getMostCurrentMove() {
            let allPieces = self.pieceController.pieces
            if self.configuration.contains(.polyglot) {
                self.polyglot.update(
                    with: lastMove,
                    castleRights: self.pieceController.castleRights,
                    pieces: allPieces
                )
            }
            if self.configuration.contains(.fen) {
                let currentMoveNum = self.historyController.getCurrentMoveNumber()
                self.fen.update(
                    turnNum: piece.color == .black ? currentMoveNum + 1 : currentMoveNum,
                    move: lastMove,
                    piece: piece,
                    castleRights: self.pieceController.castleRights,
                    pieces: allPieces
                )
            }
            if self.configuration.contains(.san) {
                var checkStatus: SAN.CheckStatus?
                if self.pieceController.isCheckmate(enpassantCoord: self.historyController.getEnpassantMoveIfAvailable()) {
                    checkStatus = .checkmate
                } else if self.pieceController.check != nil {
                    checkStatus = .check
                }
                self.san.update(move: lastMove, conflictingCoord: conflicting, checked: checkStatus)
            }
        }
        self.turn = !self.turn
        return getLastMove()
    }
    
    public func moveBack() {
        guard let mostCurrentMove = self.historyController.getMostCurrentMove() else { return }
        let currentMoveNum = self.historyController.getCurrentMoveNumber()
        self.historyController.moveBack()
        
        let (takePiece, takeCoord, promotion) = self.historyController.getPromotionAndTakeInformation(for: mostCurrentMove)
        
        let moveBeforeLast = self.historyController.getMostCurrentMove()
        self.pieceController.moveBack(
            from: mostCurrentMove.to,
            to: mostCurrentMove.from,
            takePiece: takePiece,
            takeCoord: takeCoord,
            enpassantCoord: self.historyController.getEnpassantMoveIfAvailable(),
            promotion: promotion,
            moveBeforeLastMovedTo: moveBeforeLast?.to
        )
        let previousMove = self.historyController.getMostCurrentMove()
        if self.configuration.contains(.polyglot) {
            self.polyglot.revert(
                with: mostCurrentMove,
                castleRights: self.pieceController.castleRights,
                pieces: self.pieceController.pieces,
                previousMove: previousMove
            )
        }
        if self.configuration.contains(.fen) {
            self.fen.revert(
                from: mostCurrentMove.to,
                to: mostCurrentMove.from,
                turnNum: currentMoveNum,
                previousMove: previousMove,
                pieceInfo: mostCurrentMove.pieceInfo,
                castleRights: self.pieceController.castleRights,
                pieces: self.pieceController.pieces
            )
        }
        if self.configuration.contains(.san) {
            self.san.revert()
        }
        self.turn = !self.turn
    }
    
    public func moveForward() {
        guard let nextMove = self.historyController.getNextMove() else { return }
        var promotion: PromotionSelection? = nil
        if case let .promotion(promotionSelection, _, _, _) = nextMove.type {
            promotion = promotionSelection
        }
        move(from: nextMove.from, to: nextMove.to, promotion: promotion)
    }
    
    public func moveIsEqualToNextMove(from fromCoord: Coordinate, to toCoord: Coordinate, promotion: PromotionSelection?) -> Bool {
        guard let move = getChessMove(from: fromCoord, to: toCoord, promotion: promotion), let nextMove = self.historyController.getNextMove() else { return false }
        return move == nextMove
    }
    
    public func preload(with pgn: PGN) {
        preload(with: pgn.notations)
    }
    
    public func preload(with notations: [String]) {
        reset()
        notations.forEach { notation in
            guard let (from, to, _) = moveFromNotation(notation: notation) else {
                return
            }
            move(from: from, to: to)
        }
        reset()
    }
    
}

// methods for data population
extension Game {

    //translates formal notation (e.g. Be2, Kxe5, e4) to moves
    public func moveFromNotation(notation: String) -> (Coordinate, Coordinate, PromotionSelection?)? {
        var oldCoordinate: Coordinate
        var newCoordinate: Coordinate
        var promotion: PromotionSelection? = nil
        if notation != "O-O" && notation != "O-O-O" {
            do {
                let regex = try NSRegularExpression(pattern: "(K|Q|R|B|N)?([a-h]|[1-8])?x?([a-h][1-8])(?:=([Q|R|B|N]))?(\\+|#)?", options: [])
                guard let match = regex.matches(in: notation, options: [], range: NSRange(location:0, length: notation.count)).first else {return nil}
                let ranges = (1..<match.numberOfRanges).map {Range(match.range(at: $0))}
                newCoordinate = Utils.not2Coord(notation: notation.substring(withIntRange: ranges[2]!))

                // Filter possible pieces to ones where the color is equal to the turn
                // Further filter the pieces where the notation matches the piece type
                // e.g. "B" == Bishop, "N" == Knight, "Q" == Queen, etc
                let pieceNotation = ranges[0] != nil ? notation.substring(withIntRange: ranges[0]!) : ""
                let possiblePieceCoords: LazySequence<[Coordinate]> = Coordinate.allCases.compactMap({
                    guard let piece = piece(at: $0),
                        piece.color == self.turn,
                        legalMoves(forPieceAt: $0).contains(newCoordinate.uint64Val),
                        SAN.notation(for: piece) == pieceNotation else { return nil }
                    return $0
                }).lazy

                // now filter the pieces by location
                let pieceLocation : String? = ranges[1] != nil ? notation.substring(withIntRange: ranges[1]!) : nil
                var initialCoord: Coordinate? = nil
                if let pieceLocation = pieceLocation {
                    if let fileNum = Constants.letter2NumDict[pieceLocation] {
                        let file = File(rawValue: fileNum)!
                        initialCoord = possiblePieceCoords.first(where: { $0.file == file })
                    } else if let rankNum = Int(pieceLocation) {
                        let rank = Rank(rawValue: rankNum)!
                        initialCoord = possiblePieceCoords.first(where: {$0.rank == rank})
                    }
                }
                guard let nonNilInitialCoord = initialCoord ?? possiblePieceCoords.first else { return nil }
                oldCoordinate = nonNilInitialCoord
                
                if let promotionRange = ranges[3] {
                    let promotionNotation = notation.substring(withIntRange: promotionRange)
                    switch promotionNotation {
                    case "Q":
                        promotion = .queen
                    case "B":
                        promotion = .bishop
                    case "R":
                        promotion = .rook
                    case "N":
                        promotion = .knight
                    default:
                        break
                    }
                }
            } catch {
                return nil
            }
        } else {
            guard let kingCoord = Coordinate.allCases.first(where: {
                guard let king = piece(at: $0) as? King, king.color == self.turn else { return false }
                return true
            }) else { return nil }
            let file = notation == "O-O" ? (kingCoord.file + 2)! : (kingCoord.file - 2)!
            newCoordinate = Coordinate(file: file, rank: kingCoord.rank)
            oldCoordinate = kingCoord
        }
        return (oldCoordinate, newCoordinate, promotion)
    }
}
