//
//  Game+FEN.swift
//  SwiftyChess
//
//  Created by Casey Evanish on 7/19/20.
//

import Foundation

extension Game {
    
    public func getCurrentFENString() -> String {
        precondition(self.configuration.contains(.fen), "The configuration must contain fen to get the FEN string")
        return self.fen.string
    }
    
}
