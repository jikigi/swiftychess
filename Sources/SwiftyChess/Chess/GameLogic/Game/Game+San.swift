//
//  Game+San.swift
//  SwiftyChess
//
//  Created by Casey Evanish on 7/14/20.
//

import Foundation

// SAN related code
extension Game {
    
    public func getSanAttributedString(
        moveSan san: String, moveNumber: Int, highlightedAttributes: [NSAttributedString.Key: Any],
        regularAttributes: [NSAttributedString.Key: Any]
    ) -> NSAttributedString {
        precondition(self.configuration.contains(.san), "The configuration must contain san to obtain the san")
        return self.san.getAttributedAllSan(
            moveSan: san,
            moveNumber: moveNumber,
            highlightedAttributes: highlightedAttributes,
            regularAttributes: regularAttributes
        )
    }
    
    public func getEveryMoveSan() -> [String] {
        precondition(self.configuration.contains(.san), "The configuration must contain san to obtain the san")
        return self.san.getEveryMoveSan()
    }
    
    public func getSanString() -> String {
        precondition(self.configuration.contains(.san), "The configuration must contain san to obtain the san")
        return self.san.getSanString()
    }
    
    public func getNextSan() -> String? {
        precondition(self.configuration.contains(.san), "The configuration must contain san to obtain the san")
        return self.san.getNextSan()
    }
    
    public func getLastSan() -> String? {
        precondition(self.configuration.contains(.san), "The configuration must contain san to obtain the san")
        return self.san.getLastSan()
    }
    
    public func getSan<T: RangeExpression>(range: T) -> String where T.Bound == Int {
        precondition(self.configuration.contains(.san), "The configuration must contain san to obtain the san")
        return self.san.getSan(range: range)
    }
    
}
