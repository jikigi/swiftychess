//
//  PGKey.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 2/10/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation

// http://hgm.nubati.net/book_format.html
public class PGKey {
    public var key: UInt64 = Constants.startingPGKey
    var zobrist: UInt64 = Constants.startingZobrist
    
    func update(with move: SingleChessMove, castleRights: Castling, pieces: [ChessPiece?]) {
        var pgKey = computeZobrist(from: move)
        pgKey ^= computeCastleKey(from: castleRights)
        pgKey ^= computeEnpassantKey(from: move, pieces: pieces)
        pgKey ^= computeTurnKey(from: !move.pieceInfo.color)
        self.key = pgKey
    }
    
    func revert(with move: SingleChessMove, castleRights: Castling, pieces: [ChessPiece?], previousMove: SingleChessMove?) {
        var pgKey = computeZobrist(from: move)
        pgKey ^= computeCastleKey(from: castleRights)
        if let previousMove = previousMove {
            pgKey ^= computeEnpassantKey(from: previousMove, pieces: pieces)
        }
        pgKey ^= computeTurnKey(from: move.pieceInfo.color)
        self.key = pgKey
    }
    
    private func computeZobrist(from move: SingleChessMove) -> UInt64 {
        let start = move.from
        let end = move.to
        let pieceInfo = move.pieceInfo
        let moveType = move.type
        self.zobrist ^= Zobrist.randomPiece[(64*Zobrist.getChessPieceValue(pieceType: pieceInfo.type, color: pieceInfo.color)) + (8 * (start.rank.bitOffset)) + (start.file.bitOffset)]
        
        if case let .take(takenPieceInfo, takenCoord) = moveType {
            self.zobrist ^= Zobrist.randomPiece[(64*Zobrist.getChessPieceValue(pieceType: takenPieceInfo.type, color: takenPieceInfo.color)) + (8 * (takenCoord.rank.bitOffset)) + (takenCoord.file.bitOffset)]
        } else if case let .promotion(_, _, takenPieceInfo, takenCoord) = moveType {
            if let takenPieceInfo = takenPieceInfo, let takenCoord = takenCoord {
                self.zobrist ^= Zobrist.randomPiece[(64*Zobrist.getChessPieceValue(pieceType: takenPieceInfo.type, color: takenPieceInfo.color)) + (8 * (takenCoord.rank.bitOffset)) + (takenCoord.file.bitOffset)]
            }
        }
        
        if case let .promotion(promotionSelection, color, _, _) = moveType {
            var pieceType: PieceType
            switch promotionSelection {
            case .bishop:
                pieceType = .bishop
            case .knight:
                pieceType = .knight
            case .queen:
                pieceType = .queen
            case .rook:
                pieceType = .rook
            }
            self.zobrist ^= Zobrist.randomPiece[(64*Zobrist.getChessPieceValue(pieceType: pieceType, color: color)) + (8 * (end.rank.bitOffset)) + (end.file.bitOffset)]
        } else {
            self.zobrist ^= Zobrist.randomPiece[(64*Zobrist.getChessPieceValue(pieceType: pieceInfo.type, color: pieceInfo.color)) + (8 * (end.rank.bitOffset)) + (end.file.bitOffset)]
        }
        
        if case let .castle(direction) = moveType {
            var oldFile : File
            var newFile : File
            let rank : Rank = pieceInfo.color == .black ? .RANK_8 : .RANK_1
            switch direction {
            case .king:
                (oldFile, newFile) = end.file == .FILE_G ? (.FILE_H, .FILE_F) : (.FILE_F, .FILE_H)
            case .queen:
                (oldFile, newFile) = end.file == .FILE_C ? (.FILE_A, .FILE_D) : (.FILE_D, .FILE_A)
            }
            self.zobrist ^= Zobrist.randomPiece[(64*Zobrist.getChessPieceValue(pieceType: .rook, color: pieceInfo.color)) + (8 * (rank.bitOffset)) + (oldFile.bitOffset)]
            self.zobrist ^= Zobrist.randomPiece[(64*Zobrist.getChessPieceValue(pieceType: .rook, color: pieceInfo.color)) + (8 * (rank.bitOffset)) + (newFile.bitOffset)]
        }
        return self.zobrist
    }
    
    private func computeCastleKey(from castleRights: Castling) -> UInt64 {
        var key: UInt64 = 0
        if castleRights[.white, .king] {
            key ^= Zobrist.randomCastle[0]
        }
        if castleRights[.white, .queen] {
            key ^= Zobrist.randomCastle[1]
        }
        if castleRights[.black, .king] {
            key ^= Zobrist.randomCastle[2]
        }
        if castleRights[.black, .queen] {
            key ^= Zobrist.randomCastle[3]
        }
        return key
    }
    
    private func computeEnpassantKey(from move: SingleChessMove, pieces: [ChessPiece?]) -> UInt64 {
        var key: UInt64 = 0
        let pieceInfo = move.pieceInfo
        let start = move.from
        let end = move.to
        
        if pieceInfo.type == .pawn, abs(end.rank - start.rank) == 2 {
            if let leftCoord = Coordinate(file: end.file - 1, rank: end.rank),
                let leftPiece = pieces[leftCoord.intVal] as? Pawn, leftPiece.color != pieceInfo.color {
                key ^= Zobrist.randomEnpassant[end.file.bitOffset]
            } else if let rightCoord = Coordinate(file: end.file + 1, rank: end.rank), let rightPiece = pieces[rightCoord.intVal] as? Pawn, rightPiece.color != pieceInfo.color {
                key ^= Zobrist.randomEnpassant[end.file.bitOffset]
            }
        }
        
        return key
    }
    
    private func computeTurnKey(from turnColor: Color) -> UInt64 {
        var key: UInt64 = 0
        if turnColor == .white {
            key ^= Zobrist.randomTurn
        }
        return key
    }
}
