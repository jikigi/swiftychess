//
//  SAN.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 2/24/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation

public class SAN {
    
    public enum CheckStatus {
        case check
        case checkmate
    }
    
    private var history: ChessHistoryCollection = ChessHistoryCollection<String>()
    
    func update(move: SingleChessMove, conflictingCoord: Coordinate?, checked: CheckStatus?) {
        let pieceInfo = move.pieceInfo
        let oldPos = move.from
        let newPos = move.to
        
        var san = ""
        
        let (pieceRep, coordRep) = generateMoveSan(pieceType: pieceInfo.type,
                                                   from: oldPos,
                                                   to: newPos,
                                                   conflictingCoord: conflictingCoord)
        switch move.type {
        case let .castle(direction):
            switch direction {
            // make sure to add non-breaking hyphens
            case .king:
                san += "O\u{2011}O"
            case .queen:
                san += "O\u{2011}O\u{2011}O"
            }
        case .normal:
            san += pieceRep
            san += coordRep
        case .take(_, _):
            if pieceInfo.type == .pawn {
                san += oldPos.file.letterRepresentation
            } else {
                san += pieceRep
            }
            san += "x"
            san += coordRep
        case .promotion(let promotion, _, _, let takeCoord):
            if takeCoord != nil {
                san += "\(oldPos.file.letterRepresentation)x\(coordRep)"
            } else {
                san += "\(pieceRep)\(coordRep)"
            }
            san += "="
            switch promotion {
            case .bishop:
                san += "B"
            case .knight:
                san += "N"
            case .queen:
                san += "Q"
            case .rook:
                san += "R"
            }
        }
        
        if let check = checked {
            switch check {
            case .check:
                san += "+"
            case .checkmate:
                san += "#"
            }
        }
        
        if let nextElement = self.history.getNextElement(), nextElement == san {
            self.history.incrementCurrentIndex()
            return
        }
        self.history.push(element: san)
    }
    
    func reset() {
        self.history.moveToBeginning()
    }
    
    func revert() {
        self.history.decrementHead()
    }
    
    func getMostCurrentSan() -> String? {
        return self.history.peek()
    }
    
    func getNextSan() -> String? {
        return self.history.getNextElement()
    }
    
    func getLastSan() -> String? {
        return self.history.peek()
    }
    
    // the range is in terms of each individual chess move
    func getSan<T: RangeExpression>(range: T) -> String where T.Bound == Int {
        var allSan = zip(self.history.indices[range], self.history[range]).reduce(into: "") { (result, arg) in
            let (offset, san) = arg
            if offset.isEven {
                result += "\((offset / 2) + 1). "
            }
            result += "\(san) "
        }
        if !allSan.isEmpty {
            allSan.removeLast()
        }
        return allSan
    }
    
    func getSanString() -> String {
        return getSan(range: 0..<self.history.count)
    }
    
    func getEveryMoveSan() -> [String] {
        return self.history.map { $0 }
    }
    
    // move number is in terms of chess turn (1: e4 e5 2: d4 d5) instead of (1: e4 2: e5 3: d4 4: d5)
    func getAttributedAllSan(
        moveSan moveString: String, moveNumber: Int, highlightedAttributes: [NSAttributedString.Key: Any],
        regularAttributes: [NSAttributedString.Key: Any]
    ) -> NSAttributedString {
        var moveFoundRange: NSRange?
        var currentMoveNumber = 1
        let regularString = self.history.enumerated().reduce(into: "") { (result, arg) in
            let (offset, san) = arg
            if offset.isEven {
                currentMoveNumber = (offset / 2) + 1
                result += "\(currentMoveNumber). "
            }
            if currentMoveNumber == moveNumber && san == moveString {
                moveFoundRange = NSRange(location: result.count, length: moveString.count)
            }
            result += "\(san) "
        }
        let attrString = NSMutableAttributedString(string: regularString, attributes: regularAttributes)
        if let range = moveFoundRange {
            attrString.setAttributes(highlightedAttributes, range: range)
        }
        return attrString
    }
    
}

extension SAN {
    
    private func generateMoveSan(
        pieceType: PieceType, from fromCoord: Coordinate,
        to toCoord: Coordinate, conflictingCoord: Coordinate?
    ) -> (String, String) {
        var pieceSan = SAN.notation(for: pieceType)
        if pieceType != .pawn {
            if let conflictingCoord = conflictingCoord {
                if conflictingCoord.file == fromCoord.file {
                    pieceSan += "\(String(fromCoord.rank.rawValue))"
                } else {
                    pieceSan += "\(fromCoord.file.letterRepresentation)"
                }
            }
        }
        return (pieceSan, toCoord.toNotation())
    }
}

extension SAN {
    
    public static func notation(for piece: ChessPiece) -> String {
        if piece is Pawn {
            return notation(for: .pawn)
        } else if piece is Bishop {
            return notation(for: .bishop)
        } else if piece is Knight {
            return notation(for: .knight)
        } else if piece is Rook {
            return notation(for: .rook)
        } else if piece is Queen {
            return notation(for: .queen)
        } else {
            return notation(for: .king)
        }
    }
    
    static func notation(for pieceType: PieceType) -> String {
        switch pieceType {
        case .pawn:
            return ""
        case .bishop:
            return "B"
        case .knight:
            return "N"
        case .rook:
            return "R"
        case .queen:
            return "Q"
        case .king:
            return "K"
        }
    }
    
}
