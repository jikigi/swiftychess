//
//  GameTypes.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 5/19/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation

public enum CastleDirection {
    case king
    case queen
}

public struct Castling {
    private var whiteQueen: Bool = true
    private var whiteKing: Bool = true
    private var blackQueen: Bool = true
    private var blackKing: Bool = true
    
    subscript(color: Color, direction: CastleDirection) -> Bool {
        get {
            switch color {
            case .black:
                switch direction {
                case .king:
                    return self.blackKing
                case .queen:
                    return self.blackQueen
                }
            case .white:
                switch direction {
                case .king:
                    return self.whiteKing
                case .queen:
                    return self.whiteQueen
                }
            }
        }
        set(newValue) {
            switch color {
            case .black:
                switch direction {
                case .king:
                    self.blackKing = newValue
                case .queen:
                    self.blackQueen = newValue
                }
            case .white:
                switch direction {
                case .king:
                    self.whiteKing = newValue
                case .queen:
                    self.whiteQueen = newValue
                }
            }
        }
    }
}

public enum Color: CaseIterable, Codable {
    case black
    case white
    
    public static prefix func !(_ color: Color) -> Color {
        return color == .white ? .black : .white
    }
    
    public static func fromString(string: String) -> Color {
        return string == "b" ? .black : .white
    }
    
    public func toString() -> String {
        return self == .black ? "b" : "w"
    }
}

public enum PromotionSelection {
    case queen, rook, bishop, knight
}


