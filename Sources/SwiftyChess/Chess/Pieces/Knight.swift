//
//  Knight.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 12/30/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import Foundation

public struct Knight: ChessPiece {
    
    public var color: Color
    public var movedNum: Int
    
    init(color: Color) {
        self.init(color: color, movedNum: 0)
    }

    init(color: Color, movedNum: Int) {
        self.color = color
        self.movedNum = movedNum
    }
    
    public func allAttackMoves(from coordinate: Coordinate, enpassantCoord: Coordinate?) -> UInt64 {
        return noNOEa(position: coordinate) |
            noEaEa(position: coordinate) |
            soEaEa(position: coordinate) |
            soSoEa(position: coordinate) |
            noNoWe(position: coordinate) |
            noWeWe(position: coordinate) |
            soWeWe(position: coordinate) |
            soSoWe(position: coordinate)
    }
    
    public func trimmedAttackMoves(from coordinate: Coordinate, enpassantCoord: Coordinate?, bitboards: Bitboard) -> UInt64 {
        return allAttackMoves(from: coordinate, enpassantCoord: enpassantCoord)
    }
    
    public func legalMoves(at coordinate: Coordinate, enpassantCoord: Coordinate?, pieceMap: [ChessPiece?], bitboards: Bitboard, castleRights: Castling) -> UInt64 {
        let trimmedMoves = trimmedAttackMoves(from: coordinate, enpassantCoord: enpassantCoord, bitboards: bitboards)
        return trimmedMoves & ~bitboards[self.color]
    }
    
    public func movesAttacking(piece: ChessPiece, at pieceCoordinate: Coordinate, given legalMoves: UInt64, enpassantCoord: Coordinate?) -> UInt64 {
        guard piece.color != self.color else { return 0 }
        return legalMoves & pieceCoordinate.uint64Val
    }
    
}


/*
        noNoWe    noNoEa
            +15  +17
             |     |
noWeWe  +6 __|     |__+10  noEaEa
              \   /
               >0<
           __ /   \ __
soWeWe -10   |     |   -6  soEaEa
             |     |
            -17  -15
        soSoWe    soSoEa
*/
// https://www.chessprogramming.org/Knight_Pattern
// MARK: Calculating moves
extension Knight {
    
    /*
     U64 noNoEa(U64 b) {return (b << 17) & notAFile ;}
     U64 noEaEa(U64 b) {return (b << 10) & notABFile;}
     U64 soEaEa(U64 b) {return (b >>  6) & notABFile;}
     U64 soSoEa(U64 b) {return (b >> 15) & notAFile ;}
     U64 noNoWe(U64 b) {return (b << 15) & notHFile ;}
     U64 noWeWe(U64 b) {return (b <<  6) & notGHFile;}
     U64 soWeWe(U64 b) {return (b >> 10) & notGHFile;}
     U64 soSoWe(U64 b) {return (b >> 17) & notHFile ;}
     */
    
    private func noNOEa(position: Coordinate) -> UInt64 {
        return (position.uint64Val << 17) & ~Bitboard.aFile
    }
    
    private func noEaEa(position: Coordinate) -> UInt64 {
        return (position.uint64Val << 10) & ~Bitboard.abFile
    }
    
    private func soEaEa(position: Coordinate) -> UInt64 {
        return (position.uint64Val >> 6) & ~Bitboard.abFile
    }
    
    private func soSoEa(position: Coordinate) -> UInt64 {
        return (position.uint64Val >> 15) & ~Bitboard.aFile
    }
    
    private func noNoWe(position: Coordinate) -> UInt64 {
        return (position.uint64Val << 15) & ~Bitboard.hFile
    }
    
    private func noWeWe(position: Coordinate) -> UInt64 {
        return (position.uint64Val << 6) & ~Bitboard.ghFile
    }
    
    private func soWeWe(position: Coordinate) -> UInt64 {
        return (position.uint64Val >> 10) & ~Bitboard.ghFile
    }
    
    private func soSoWe(position: Coordinate) -> UInt64 {
        return (position.uint64Val >> 17) & ~Bitboard.hFile
    }
    
}
