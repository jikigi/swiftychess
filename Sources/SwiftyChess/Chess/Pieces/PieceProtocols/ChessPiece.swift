//
//  ChessPiece.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 12/30/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import Foundation

public protocol ChessPiece {
    var color: Color { get set }
    var movedNum: Int { get set }
    // every single attack moves regardless of the pieces on the board
    func allAttackMoves(from: Coordinate, enpassantCoord: Coordinate?) -> UInt64
    // trimmed attack moves. Takes into account the pieces on the board
    // mostly used to see what pieces this piece attacks/supports
    func trimmedAttackMoves(from: Coordinate, enpassantCoord: Coordinate?, bitboards: Bitboard) -> UInt64
    // legal moves the piece can make
    func legalMoves(at coordinate: Coordinate,
                    enpassantCoord: Coordinate?,
                    pieceMap: [ChessPiece?],
                    bitboards: Bitboard,
                    castleRights: Castling) -> UInt64
    // the moves that are attacking a certain piece at position
    // my attempt at attempting en passant captures cleanly
    func movesAttacking(piece: ChessPiece,
                        at pieceCoordinate: Coordinate,
                        given legalMoves: UInt64,
                        enpassantCoord: Coordinate?) -> UInt64
}

extension ChessPiece {
    var hasntMoved: Bool {
        self.movedNum == 0
    }
}
