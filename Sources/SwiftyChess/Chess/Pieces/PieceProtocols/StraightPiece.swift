//
//  StraightPiece.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 1/6/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation

internal enum StraightDirection: CaseIterable {
    case top
    case bottom
    case left
    case right
}

protocol StraightPiece {}

//https://www.chessprogramming.org/On_an_empty_Board
extension StraightPiece where Self: ChessPiece & SlidingPiece {
    
    func getVerticalAndHorizontalMask(from currentPosition: Coordinate) -> UInt64 {
        return getHorizontalMask(coordinate: currentPosition)
            | getVerticalMask(coordinate: currentPosition)
    }
    
    func getHorizontalMask(coordinate: Coordinate) -> UInt64 {
        return  UInt64(0xff) << (coordinate.rawValue & 56)
    }
    
    func getVerticalMask(coordinate: Coordinate) -> UInt64 {
        return UInt64(0x0101010101010101) << (coordinate.rawValue & 7)
    }
    
    func getMask(coordinate: Coordinate, for direction: StraightDirection) -> UInt64 {
        switch direction {
        case .top:
            return UInt64(0x0101010101010100) << coordinate.rawValue
        case .bottom:
            return UInt64(0x0080808080808080) >> (coordinate.rawValue ^ 63)
        case .left:
            let one: UInt64 = 1
            return UInt64((one << coordinate.rawValue) - (one << (coordinate.rawValue & 56)))
        case .right:
            let one: UInt64 = 1
            return UInt64(2)*((one << (coordinate.rawValue|7)) - (one << coordinate.rawValue))
        }
    }
    
    func getTrimmedStraightMoves(coordinate: Coordinate, bitboard: UInt64) -> UInt64 {
        let vertMask = getVerticalMask(coordinate: coordinate)
        let horzMask = getHorizontalMask(coordinate: coordinate)
        return obstructionDifference(mask: vertMask, bitboard: bitboard, coordinate: coordinate)
            | obstructionDifference(mask: horzMask, bitboard: bitboard, coordinate: coordinate)
    }
    
    func getStraightPinnableRange(from coord: Coordinate, to kingCoord: Coordinate) -> UInt64? {
        var containedStr8: UInt64
        let kingUInt = kingCoord.uint64Val
        let horz = getHorizontalMask(coordinate: coord)
        if !horz.contains(kingUInt) {
            let vert = getVerticalMask(coordinate: coord)
            guard vert.contains(kingUInt) else { return nil }
            containedStr8 = vert
        } else {
            containedStr8 = horz
        }
        let mask = (UInt64.max << coord.rawValue) ^ (UInt64.max << kingCoord.rawValue)
        var moves = mask & containedStr8
        moves.remove(coord.uint64Val)
        moves.remove(kingUInt)
        return moves
    }
}
