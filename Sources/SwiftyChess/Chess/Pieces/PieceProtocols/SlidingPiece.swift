//
//  PinnablePiece.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 1/6/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation

protocol SlidingPiece {
    // gets the bitmap from the coordinate to the king coordinate where the piece can move
    func getPossiblePinRange(from coord: Coordinate,
                             to kingCoord: Coordinate) -> UInt64?
}

extension SlidingPiece where Self: ChessPiece {
    
    func inSight(of coordinate: Coordinate, from currCoordinate: Coordinate) -> Bool {
        return (allAttackMoves(from: currCoordinate, enpassantCoord: nil) & coordinate.uint64Val) != 0
    }
    
    // https://www.chessprogramming.org/Obstruction_Difference
    func obstructionDifference(mask: UInt64, bitboard: UInt64, coordinate: Coordinate) -> UInt64 {
        var mask = mask
        let coordinateUInt = coordinate.uint64Val
        mask.remove(coordinateUInt)
        let occupancyXMask: UInt64 = mask & bitboard
        let occupancyUpper: UInt64 = (UInt64.max << coordinate.intVal) & occupancyXMask
        let occupancyLower: UInt64 = occupancyXMask ^ occupancyUpper
        let lsb1Upper: UInt64 = 0x1 << occupancyUpper.trailingZeroBitCount
        let msb1Lower: UInt64 = 0x8000000000000000 >> (occupancyLower | UInt64(1)).leadingZeroBitCount
        return ((2 &* lsb1Upper) &- msb1Lower) & mask
    }
    
}
