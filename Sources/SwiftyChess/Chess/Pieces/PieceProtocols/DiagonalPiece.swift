//
//  DiagonalPiece.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 1/6/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation

protocol DiagonalPiece {}

internal enum DiagonalDirection: CaseIterable {
    case topRight
    case topLeft
    case bottomRight
    case bottomLeft
}
extension DiagonalPiece where Self: ChessPiece & SlidingPiece {
    
    func diagonal(coordinate: Coordinate) -> UInt64 {
        let mainDiagonal: UInt64 = 0x8040201008040201
        let diag = 8 * (coordinate.rawValue & 7) - (coordinate.rawValue & 56)
        let north = -diag & ( diag >> 31)
        let south = diag & (-diag >> 31)
        return (mainDiagonal >> south) << north
    }

    func antiDiagonal(coordinate: Coordinate) -> UInt64 {
        let mainAntiDiag: UInt64 = 0x0102040810204080
        let antiDiag = 56 - 8 * (coordinate.rawValue & 7) - (coordinate.rawValue & 56)
        let antiNorth = -antiDiag & ( antiDiag >> 31)
        let antiSouth =  antiDiag & (-antiDiag >> 31)
        return (mainAntiDiag >> antiSouth) << antiNorth
    }
    
    func fullDiagonals(from coordinate: Coordinate) -> UInt64 {
        return (diagonal(coordinate: coordinate) |
            antiDiagonal(coordinate: coordinate)) ^ coordinate.uint64Val
    }
    
    func getTrimmedDiagonalMoves(coordinate: Coordinate, bitboard: UInt64) -> UInt64 {
        let diag = diagonal(coordinate: coordinate)
        let antiDiag = antiDiagonal(coordinate: coordinate)
        return obstructionDifference(mask: diag, bitboard: bitboard, coordinate: coordinate)
            | obstructionDifference(mask: antiDiag, bitboard: bitboard, coordinate: coordinate)
    }
    
    func getDiagonalPinnableRange(from coord: Coordinate, to kingCoord: Coordinate) -> UInt64? {
        var containedDiag: UInt64
        let kingUInt = kingCoord.uint64Val
        let diag = diagonal(coordinate: coord)
        if !diag.contains(kingUInt) {
            let antiDiag = antiDiagonal(coordinate: coord)
            guard antiDiag.contains(kingUInt) else { return nil }
            containedDiag = antiDiag
        } else {
            containedDiag = diag
        }
        let mask = (UInt64.max << coord.rawValue) ^ (UInt64.max << kingCoord.rawValue)
        var moves = mask & containedDiag
        moves.remove(coord.uint64Val)
        moves.remove(kingUInt)
        return moves
    }
}
