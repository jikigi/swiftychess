//
//  Bishop.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 12/30/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import Foundation

public struct Bishop: ChessPiece, DiagonalPiece, SlidingPiece {
    
    public var color: Color
    public var movedNum: Int
    
    init(color: Color) {
        self.init(color: color, movedNum: 0)
    }

    init(color: Color, movedNum: Int) {
        self.color = color
        self.movedNum = movedNum
    }
    
    public func allAttackMoves(from coordinate: Coordinate, enpassantCoord: Coordinate?) -> UInt64 {
        return self.fullDiagonals(from: coordinate)
    }
    
    public func trimmedAttackMoves(from coordinate: Coordinate, enpassantCoord: Coordinate?, bitboards: Bitboard) -> UInt64 {
        let bitboard = bitboards[self.color] | bitboards[!self.color]
        return getTrimmedDiagonalMoves(coordinate: coordinate, bitboard: bitboard)
    }
    
    public func legalMoves(at coordinate: Coordinate, enpassantCoord: Coordinate?, pieceMap: [ChessPiece?], bitboards: Bitboard, castleRights: Castling) -> UInt64 {
        let trimmedMoves = trimmedAttackMoves(from: coordinate, enpassantCoord: enpassantCoord, bitboards: bitboards)
        return trimmedMoves & ~bitboards[self.color]
    }
    
    func getPossiblePinRange(from coord: Coordinate, to kingCoord: Coordinate) -> UInt64? {
        return self.getDiagonalPinnableRange(from: coord, to: kingCoord)
    }
    
    public func movesAttacking(piece: ChessPiece, at pieceCoordinate: Coordinate, given legalMoves: UInt64, enpassantCoord: Coordinate?) -> UInt64 {
        guard piece.color != self.color else { return 0 }
        return legalMoves & pieceCoordinate.uint64Val
    }
}
