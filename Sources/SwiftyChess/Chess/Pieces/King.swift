//
//  King.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 12/31/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import Foundation

public struct King: ChessPiece {
    
    public var color: Color
    public var movedNum: Int
    
    init(color: Color) {
        self.init(color: color, movedNum: 0)
    }

    init(color: Color, movedNum: Int) {
        self.color = color
        self.movedNum = movedNum
    }
    
    public func allAttackMoves(from coordinate: Coordinate, enpassantCoord: Coordinate?) -> UInt64 {
        var allPossibleMoves: UInt64 = 0
        
        let currentRank: UInt8 = 1 << coordinate.file.bitOffset
        let possibleMovesRank: UInt8 = currentRank | (currentRank << 1) | (currentRank >> 1)
        
        if let topRank = coordinate.rank + 1 {
            allPossibleMoves |= (UInt64(possibleMovesRank) << (topRank.bitOffset * 8))
        }
        allPossibleMoves |= (UInt64(possibleMovesRank) << (coordinate.rank.bitOffset * 8))
        if let bottomRank = coordinate.rank - 1 {
            allPossibleMoves |= (UInt64(possibleMovesRank) << (bottomRank.bitOffset * 8))
        }
        
        return allPossibleMoves
    }
    
    public func trimmedAttackMoves(from coordinate: Coordinate, enpassantCoord: Coordinate?, bitboards: Bitboard) -> UInt64 {
        return allAttackMoves(from: coordinate, enpassantCoord: enpassantCoord)
    }
    
    public func legalMoves(at coordinate: Coordinate, enpassantCoord: Coordinate?, pieceMap: [ChessPiece?], bitboards: Bitboard, castleRights: Castling) -> UInt64 {
        var pieceMap = pieceMap
        var bitboards = bitboards
        var possibleMoves = allAttackMoves(from: coordinate, enpassantCoord: enpassantCoord)
        // clear out this coordinate to account for skewer moves
        pieceMap[coordinate.intVal] = nil
        bitboards[self.color].remove(coordinate.uint64Val)
        
        let otherPiecesMoves = pieceMap
            .enumerated()
            .reduce(into: UInt64(0), { (result, arg1) in
                let (offset, piece) = arg1
                guard let nonNullPiece = piece, nonNullPiece.color != self.color else { return }
                result |= nonNullPiece.trimmedAttackMoves(from: offset.toChessCoord(), enpassantCoord: enpassantCoord, bitboards: bitboards)
            })
        if self.hasntMoved {
            let kingsideTwoMoves = (coordinate.uint64Val << 1) | (coordinate.uint64Val << 2)
            if castleRights[self.color, .king]
                && (otherPiecesMoves & kingsideTwoMoves) == 0
                && (bitboards[self.color] & kingsideTwoMoves) == 0 {
                let twoKingsideCoordinate = Coordinate(file: coordinate.file + 2, rank: coordinate.rank)!
                possibleMoves |= twoKingsideCoordinate.uint64Val
            }
            let queensideTwoMoves = (coordinate.uint64Val >> 1) | (coordinate.uint64Val >> 2)
            let queensideThreeMoves = queensideTwoMoves | (coordinate.uint64Val >> 3)
            if castleRights[self.color, .queen]
                && (otherPiecesMoves & queensideTwoMoves) == 0
                && (bitboards[self.color] & queensideThreeMoves) == 0 {
                let twoQueensideCoordinate = Coordinate(file: coordinate.file - 2, rank: coordinate.rank)!
                possibleMoves |= twoQueensideCoordinate.uint64Val
            }
        }
        
        // add this coordinate again so that the coordinate is not added in possibleMoves
        bitboards[self.color].add(coordinate.uint64Val)
        return possibleMoves & ~bitboards[self.color] & ~otherPiecesMoves
    }
    
    public func movesAttacking(piece: ChessPiece, at pieceCoordinate: Coordinate, given legalMoves: UInt64, enpassantCoord: Coordinate?) -> UInt64 {
        guard piece.color != self.color else { return 0 }
        return legalMoves & pieceCoordinate.uint64Val
    }
    
}
