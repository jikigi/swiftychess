//
//  Pawn.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 12/29/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import Foundation

public struct Pawn: ChessPiece {
    
    public var color: Color
    public var movedNum: Int
    
    init(color: Color) {
        self.init(color: color, movedNum: 0)
    }

    init(color: Color, movedNum: Int) {
        self.color = color
        self.movedNum = movedNum
    }
    
    public func allAttackMoves(from coordinate: Coordinate, enpassantCoord: Coordinate?) -> UInt64 {
        var allPossibleMoves: UInt64 = 0
        let direction = self.color == .white ? 1 : -1
        let rankOffsetByOne = coordinate.rank + direction
        
        if let diagLeft = Coordinate(file: coordinate.file - 1, rank: rankOffsetByOne) {
            allPossibleMoves.add(diagLeft.uint64Val)
        }
        
        if let diagRight = Coordinate(file: coordinate.file + 1, rank: rankOffsetByOne) {
            allPossibleMoves.add(diagRight.uint64Val)
        }
        
        return allPossibleMoves
    }
    
    public func trimmedAttackMoves(from coordinate: Coordinate, enpassantCoord: Coordinate?, bitboards: Bitboard) -> UInt64 {
        return allAttackMoves(from: coordinate, enpassantCoord: enpassantCoord)
    }
    
    public func legalMoves(at coordinate: Coordinate, enpassantCoord: Coordinate?, pieceMap: [ChessPiece?], bitboards: Bitboard, castleRights: Castling) -> UInt64 {
        let possibleMoves: UInt64 = allAttackMoves(from: coordinate, enpassantCoord: enpassantCoord)
        var trimmedMoves = possibleMoves
        // keeps squares where diff color piece exists
        trimmedMoves &= bitboards[!self.color]
        if let enPassantCoord = enpassantCoord,
            (self.color == .black && coordinate.rank == Rank.RANK_4) || (self.color == .white && coordinate.rank == Rank.RANK_5),
            abs(enPassantCoord.file - coordinate.file) == 1 {
            trimmedMoves.add(enPassantCoord.uint64Val)
        }
        
        let direction = self.color == .white ? 1 : -1
        
        // check if the coordinate one rank up is empty
        var coordinateOneRankUp = coordinate.uint64Val << (UInt8.bitWidth * direction)
        let oneRankUpPossibleMove = ~bitboards[self.color] & ~bitboards[!self.color] & coordinateOneRankUp
        trimmedMoves |= oneRankUpPossibleMove
        
        // now check the coordinate two ranks up for beginning stuff
        if self.hasntMoved {
            coordinateOneRankUp = coordinateOneRankUp << (UInt8.bitWidth * direction)
            let oneRankUpIsEmpty = oneRankUpPossibleMove != 0
            if oneRankUpIsEmpty {
                trimmedMoves |= (~bitboards[self.color] & ~bitboards[!self.color]) & coordinateOneRankUp
            }
        }
        
        return trimmedMoves
    }
    
    public func movesAttacking(piece: ChessPiece, at pieceCoordinate: Coordinate, given legalMoves: UInt64, enpassantCoord: Coordinate?) -> UInt64 {
        guard piece.color != self.color else { return 0 }
        // checks if the piece is a pawn and just made an enpassant move
        if let enpassantCoord = enpassantCoord, piece is Pawn,
            abs(pieceCoordinate.rank - enpassantCoord.rank) == 1 &&
                enpassantCoord.file == pieceCoordinate.file {
            return legalMoves & enpassantCoord.uint64Val
        }
        return legalMoves & pieceCoordinate.uint64Val
    }
    
}
