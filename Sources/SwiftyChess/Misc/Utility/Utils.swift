//
//  Utils.swift
//  SwiftyChess
//
//  Created by Casey Evanish on 11/25/19.
//

import Foundation

struct Utils {
    
    static func parseNotation(notation: String) -> (String, String, PromotionSelection?) {
        let matches = notation.capturedGroups(withRegex: "([a-h][1-8])([a-h][1-8])(q|b|k|r)?")
        var promotion: PromotionSelection?
        if matches.count > 2 {
            switch matches[2] {
            case "q":
                promotion = .queen
            case "b":
                promotion = .bishop
            case "k":
                promotion = .knight
            case "r":
                promotion = .rook
            default:
                break
            }
        }
        return (matches[0], matches[1], promotion)
    }
    
    static func not2Coord(notation: String) -> Coordinate {
        let numberDict: [String: Int] = ["a":1, "b":2, "c":3, "d":4, "e":5, "f":6, "g":7, "h":8]
        let first = notation.index(notation.startIndex, offsetBy: 1)
        let letter = String(notation[..<first])
        let file = File(rawValue: numberDict[letter]!)!
        let last = notation.index(notation.endIndex, offsetBy: -1)
        let number = String(notation[last...])
        let rank = Rank(rawValue: Int(number)!)!
        return Coordinate(file: file, rank: rank)
    }
    
}
