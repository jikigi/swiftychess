//
//  String+Extensions.swift
//  SwiftyChess
//
//  Created by Casey Evanish on 11/25/19.
//

import Foundation

extension String {
    
    func substring(withIntRange range: Range<Int>) -> String {
        let r = self.index(self.startIndex, offsetBy: range.lowerBound)..<self.index(self.startIndex, offsetBy: range.lowerBound+range.count)
        return String(self[r])
    }
    
    func capturedGroups(withRegex pattern: String) -> [String] {
        var results = [String]()
        
        var regex: NSRegularExpression
        do {
            regex = try NSRegularExpression(pattern: pattern, options: [])
        } catch {
            return results
        }
        
        let matches = regex.matches(in: self, options: [], range: NSRange(location:0, length: self.count))
        
        guard matches.first != nil else { return results }
        
        for match in matches {
            guard match.numberOfRanges > 1 else {
                guard let range = Range(match.range(at: 0), in: self) else { continue }
                results.append(String(self[range]))
                continue
            }
            for rangeIndex in 1..<match.numberOfRanges {
                guard let range = Range(match.range(at: rangeIndex), in: self) else { continue }
                results.append(String(self[range]))
            }
        }
        
        return results
    }
    
}
