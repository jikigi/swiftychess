//
//  UInt64+Extensions.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 9/17/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation

extension UInt64 {
    mutating func add(_ val: UInt64) {
        self |= val
    }
    
    mutating func remove(_ val: UInt64) {
        self &= ~val
    }
    
    func contains(_ val: UInt64) -> Bool {
        return (self & val) == val
    }
    
    func reversed() -> UInt64 {
        var selfCopy = self
        var count = 64
        var reverseNum: UInt64 = selfCopy
        while selfCopy != 0 {
            reverseNum <<= 1
            reverseNum |= selfCopy & 1
            selfCopy >>= 1
            count -= 1
        }
        reverseNum <<= count
        return reverseNum
    }
    
    var leadingSetBitOffset: Int {
        return self.bitWidth - self.leadingZeroBitCount
    }

    var trailingSetBitOffset: Int {
        let trailingZeros = self.trailingZeroBitCount
        return trailingZeros == self.bitWidth ? 0 : trailingZeros + 1
    }
    
    func toCoord() -> Coordinate {
        return Int(log2(Double(self))).toChessCoord()
    }
    
    var hasOnlyOneBitSet: Bool {
        return self != 0 && (self & (self-1)) == 0
    }
    
    
    // MARK: useful debug methods
    func toCoordinateArray() -> [Coordinate] {
        let possibleMoves: [Coordinate] = (0..<64).compactMap({
            if self & (UInt64(1) << $0) != 0 {
                return $0.toChessCoord()
            }
            return nil
        })
        return possibleMoves
    }
    
    func printBoard() {
        var reversed = self.reversed()
        (0...7).forEach({ _ in
            let string = String(reversed & 0x00000000000000FF, radix: 2)
            print(String(repeating: "0", count: 8 - string.count) + string)
            reversed >>= 8
        })
    }
}
