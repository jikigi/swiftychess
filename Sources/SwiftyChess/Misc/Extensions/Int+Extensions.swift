//
//  Int+Extensions.swift
//  SwiftyChess
//
//  Created by Casey Evanish on 11/25/19.
//

import Foundation

extension Int {
    
    ///converts an int to a Coordinate object
    ///0 -> A1, 63 -> H8
    func toChessCoord() -> Coordinate {
        return Coordinate(rawValue: self)!
    }
    
    var isEven: Bool {
        return self % 2 == 0
    }
}
