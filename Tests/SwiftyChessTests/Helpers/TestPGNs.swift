//
//  TestPGNs.swift
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 8/13/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation

class TestPGNs {
    static var enumerator: FileManager.DirectoryEnumerator? {
        return FileManager.default.enumerator(atPath: self.directory.absoluteString.replacingOccurrences(of: "file:", with: ""))
    }
    
    static var directory: URL {
        return Bundle.module.resourceURL!
    }
}
