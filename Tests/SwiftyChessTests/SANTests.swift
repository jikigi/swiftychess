//
//  SanTests.swift
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 8/12/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import XCTest
@testable import SwiftyChess

class SANTests: XCTestCase {

    func testSANWithPGN() {
        guard let enumerator = TestPGNs.enumerator else {
            XCTFail("cannot find pgn enumerator")
            return
        }
        let game = Game(configuration: [.san])
        while let nextPath = enumerator.nextObject() as? String {
            var directoryCopy = TestPGNs.directory
            directoryCopy.appendPathComponent(nextPath)
            guard directoryCopy.pathExtension == "pgn", let pgn = PGN(url: directoryCopy) else { continue }
            print("testing \(#function) for \(nextPath)")
            game.preload(with: pgn)
            for san in pgn.notations {
                let nextSan = game.san.getNextSan()!
                XCTAssertEqual(san, nextSan, "an incorrect san was found in \(nextPath)")
                game.moveForward()
            }
        }
    }
    
    func testSanAccuracy() {
        let game = loadAlekhineGame()
        let correctSan = "1. e4 e5 2. Nf3 d6 3. Bc4 Nc6 4. c3 Bg4 5. Qb3 Qd7 6. Ng5 Nh6 7. Bxf7+ Nxf7 8. Nxf7 Qxf7 9. Qxb7 Kd7 10. Qxa8 Qc4 11. f3 Bxf3 12. gxf3 Nd4 13. d3 Qxd3 14. cxd4 Be7 15. Qxh8 Bh4#"
        XCTAssertEqual(game.getSanString(), correctSan, "the full san does not match what it should be")
    }
    
    #if os(iOS)
    func testAttributedSanAccuracy() throws {
        let game = Game(configuration: [.san])
        let pgnString = "1. e4 e5 2. Nf3 d6 3. Ng1 Nc6 4. Nf3 Bg4"
        
        game.preload(with: PGN(string: pgnString))
        
        let highlightedAttributes: [NSAttributedString.Key: Any] = [.font: UIFont(name:"HelveticaNeue-Bold", size: 12)!]
        let regularAttributes: [NSAttributedString.Key: Any] = [.font: UIFont(name:"HelveticaNeue", size: 10)!]
        
        var attrString = game.getSanAttributedString(moveSan: "e4",
                                                     moveNumber: 1,
                                                     highlightedAttributes: highlightedAttributes,
                                                     regularAttributes: regularAttributes)
        var highlightedFont = try XCTUnwrap(attrString.attribute(.font, at: 3, longestEffectiveRange: nil, in: .init(location: 3, length: 2))) as! UIFont
        XCTAssertEqual(highlightedFont.fontName, "HelveticaNeue-Bold")
        XCTAssertEqual(highlightedFont.pointSize, 12)
        
        var regularFont = try XCTUnwrap(attrString.attribute(.font, at: 0, longestEffectiveRange: nil, in: .init(location: 0, length: 3))) as! UIFont
        XCTAssertEqual(regularFont.fontName, "HelveticaNeue")
        XCTAssertEqual(regularFont.pointSize, 10)
        
        regularFont = try XCTUnwrap(attrString.attribute(.font, at: 5, longestEffectiveRange: nil, in: .init(location: 5, length: 34))) as! UIFont
        XCTAssertEqual(regularFont.fontName, "HelveticaNeue")
        XCTAssertEqual(regularFont.pointSize, 10)
        
        attrString = game.getSanAttributedString(moveSan: "Nf3",
                                                 moveNumber: 4,
                                                 highlightedAttributes: highlightedAttributes,
                                                 regularAttributes: regularAttributes)
        
        highlightedFont = try XCTUnwrap(attrString.attribute(.font, at: 33, longestEffectiveRange: nil, in: .init(location: 33, length: 3))) as! UIFont
        XCTAssertEqual(highlightedFont.fontName, "HelveticaNeue-Bold")
        XCTAssertEqual(highlightedFont.pointSize, 12)
        
        regularFont = try XCTUnwrap(attrString.attribute(.font, at: 12, longestEffectiveRange: nil, in: .init(location: 12, length: 3))) as! UIFont
        XCTAssertEqual(regularFont.fontName, "HelveticaNeue")
        XCTAssertEqual(regularFont.pointSize, 10)
        
    }
    #endif
    
    func testSanRangeAccuracy() {
        let game = loadAlekhineGame()
        
        XCTAssertEqual(game.san.getSan(range: 0..<0), "")
        XCTAssertEqual(game.san.getSan(range: 0..<2), "1. e4 e5")
        XCTAssertEqual(game.san.getSan(range: 0..<1), "1. e4")
        XCTAssertEqual(game.san.getSan(range: 1..<3), "e5 2. Nf3")
        
    }
    
    private func loadAlekhineGame() -> Game {
        let game = Game(configuration: [.san])
        var directoryCopy = TestPGNs.directory
        directoryCopy.appendPathComponent("Rodzinski_vs_Alekhine Alexander A _1913.__.__.pgn")
        let pgn = PGN(url: directoryCopy)!
        game.preload(with: pgn)
        return game
    }

}
