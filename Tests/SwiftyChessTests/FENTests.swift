//
//  FENTests.swift
//  ChessOpenings
//
//  Created by Casey Evanish on 10/22/17.
//  Copyright © 2017 Casey Evanish. All rights reserved.
//

import XCTest
@testable import SwiftyChess

class FENTests: XCTestCase {
    
    var game: Game!
    
    override func setUp() {
        super.setUp()
//        self.game = Game(playAs: .white)
//        self.game.initPieceMap()
        self.game = Game()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFEN1() {
        
        //moving white pawn from e2 to e4
        testSingleMove(from: Coordinate(file: .FILE_E, rank: .RANK_2), to: Coordinate(file: .FILE_E, rank: .RANK_4), expect: "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1")
        
        //moving black pawn from d7 to d5
        testSingleMove(from: Coordinate(file: .FILE_D, rank: .RANK_7), to: Coordinate(file: .FILE_D, rank: .RANK_5), expect: "rnbqkbnr/ppp1pppp/8/3p4/4P3/8/PPPP1PPP/RNBQKBNR w KQkq d6 0 2")

        //moving white pawn from e4 to e5
        testSingleMove(from: Coordinate(file: .FILE_E, rank: .RANK_4), to: Coordinate(file: .FILE_E, rank: .RANK_5), expect: "rnbqkbnr/ppp1pppp/8/3pP3/8/8/PPPP1PPP/RNBQKBNR b KQkq - 0 2")

        //moving black pawn from f7 to f5
        testSingleMove(from: Coordinate(file: .FILE_F, rank: .RANK_7), to: Coordinate(file: .FILE_F, rank: .RANK_5), expect: "rnbqkbnr/ppp1p1pp/8/3pPp2/8/8/PPPP1PPP/RNBQKBNR w KQkq f6 0 3")

        //moving white king from e1 to e2
        testSingleMove(from: Coordinate(file: .FILE_E, rank: .RANK_1), to: Coordinate(file: .FILE_E, rank: .RANK_2), expect: "rnbqkbnr/ppp1p1pp/8/3pPp2/8/8/PPPPKPPP/RNBQ1BNR b kq - 1 3")

        //moving black king from e8 to f7
        testSingleMove(from: Coordinate(file: .FILE_E, rank: .RANK_8), to: Coordinate(file: .FILE_F, rank: .RANK_7), expect: "rnbq1bnr/ppp1pkpp/8/3pPp2/8/8/PPPPKPPP/RNBQ1BNR w - - 2 4")

        //moving white pawn from h2 to h3
        testSingleMove(from: Coordinate(file: .FILE_H, rank: .RANK_2), to: Coordinate(file: .FILE_H, rank: .RANK_3), expect: "rnbq1bnr/ppp1pkpp/8/3pPp2/8/7P/PPPPKPP1/RNBQ1BNR b - - 0 4")

        //moving black knight from g8 to f6
        testSingleMove(from: Coordinate(file: .FILE_G, rank: .RANK_8), to: Coordinate(file: .FILE_F, rank: .RANK_6), expect: "rnbq1b1r/ppp1pkpp/5n2/3pPp2/8/7P/PPPPKPP1/RNBQ1BNR w - - 1 5")

        testTakeBack(expect: "rnbq1bnr/ppp1pkpp/8/3pPp2/8/7P/PPPPKPP1/RNBQ1BNR b - - 0 4")
        testTakeBack(expect: "rnbq1bnr/ppp1pkpp/8/3pPp2/8/8/PPPPKPPP/RNBQ1BNR w - - 2 4")
        testTakeBack(expect: "rnbqkbnr/ppp1p1pp/8/3pPp2/8/8/PPPPKPPP/RNBQ1BNR b kq - 1 3")
        testTakeBack(expect: "rnbqkbnr/ppp1p1pp/8/3pPp2/8/8/PPPP1PPP/RNBQKBNR w KQkq f6 0 3")
        testTakeBack(expect: "rnbqkbnr/ppp1pppp/8/3pP3/8/8/PPPP1PPP/RNBQKBNR b KQkq - 0 2")
        testTakeBack(expect: "rnbqkbnr/ppp1pppp/8/3p4/4P3/8/PPPP1PPP/RNBQKBNR w KQkq d6 0 2")
        testTakeBack(expect: "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1")
        testTakeBack(expect: Constants.startingFEN)
//
//        testForward(expect: "rnbqkbnr/ppp1pppp/8/3p4/4P3/8/PPPP1PPP/RNBQKBNR w KQkq d6 0 2")
//        testForward(expect: "rnbqkbnr/ppp1p1pp/8/3pPp2/8/8/PPPP1PPP/RNBQKBNR w KQkq f6 0 3")
//        testForward(expect: "rnbq1bnr/ppp1pkpp/8/3pPp2/8/8/PPPPKPPP/RNBQ1BNR w - - 2 4")
//        testForward(expect: "rnbq1b1r/ppp1pkpp/5n2/3pPp2/8/7P/PPPPKPP1/RNBQ1BNR w - - 1 5")
        
    }
    
    func testFEN2() {
        //moving white pawn from a2 to a4
        testSingleMove(from: Coordinate(file: .FILE_A, rank: .RANK_2), to: Coordinate(file: .FILE_A, rank: .RANK_4), expect: "rnbqkbnr/pppppppp/8/8/P7/8/1PPPPPPP/RNBQKBNR b KQkq a3 0 1")

        //moving black pawn from b7 to b5
        testSingleMove(from: Coordinate(file: .FILE_B, rank: .RANK_7), to: Coordinate(file: .FILE_B, rank: .RANK_5), expect: "rnbqkbnr/p1pppppp/8/1p6/P7/8/1PPPPPPP/RNBQKBNR w KQkq b6 0 2")

        //moving white pawn from h2 to h4
        testSingleMove(from: Coordinate(file: .FILE_H, rank: .RANK_2), to: Coordinate(file: .FILE_H, rank: .RANK_4), expect: "rnbqkbnr/p1pppppp/8/1p6/P6P/8/1PPPPPP1/RNBQKBNR b KQkq h3 0 2")

        //moving black pawn from b5 to b4
        testSingleMove(from: Coordinate(file: .FILE_B, rank: .RANK_5), to: Coordinate(file: .FILE_B, rank: .RANK_4), expect: "rnbqkbnr/p1pppppp/8/8/Pp5P/8/1PPPPPP1/RNBQKBNR w KQkq - 0 3")

        //moving white pawn from c2 to c4
        testSingleMove(from: Coordinate(file: .FILE_C, rank: .RANK_2), to: Coordinate(file: .FILE_C, rank: .RANK_4), expect: "rnbqkbnr/p1pppppp/8/8/PpP4P/8/1P1PPPP1/RNBQKBNR b KQkq c3 0 3")

        //moving black pawn from b4 to c3
        testSingleMove(from: Coordinate(file: .FILE_B, rank: .RANK_4), to: Coordinate(file: .FILE_C, rank: .RANK_3), enpassantCoord: Coordinate(file: .FILE_C, rank: .RANK_4), expect: "rnbqkbnr/p1pppppp/8/8/P6P/2p5/1P1PPPP1/RNBQKBNR w KQkq - 0 4")

        //moving white rook from a1 to a3
        testSingleMove(from: Coordinate(file: .FILE_A, rank: .RANK_1), to: Coordinate(file: .FILE_A, rank: .RANK_3), expect: "rnbqkbnr/p1pppppp/8/8/P6P/R1p5/1P1PPPP1/1NBQKBNR b Kkq - 1 4")

        //moving black pawn from c3 to b2
        testSingleMove(from: Coordinate(file: .FILE_C, rank: .RANK_3), to: Coordinate(file: .FILE_B, rank: .RANK_2), expect: "rnbqkbnr/p1pppppp/8/8/P6P/R7/1p1PPPP1/1NBQKBNR w Kkq - 0 5")

        testTakeBack(expect: "rnbqkbnr/p1pppppp/8/8/P6P/R1p5/1P1PPPP1/1NBQKBNR b Kkq - 1 4")
        testTakeBack(expect: "rnbqkbnr/p1pppppp/8/8/P6P/2p5/1P1PPPP1/RNBQKBNR w KQkq - 0 4")
        testTakeBack(expect: "rnbqkbnr/p1pppppp/8/8/PpP4P/8/1P1PPPP1/RNBQKBNR b KQkq c3 0 3")
        testTakeBack(expect: "rnbqkbnr/p1pppppp/8/8/Pp5P/8/1PPPPPP1/RNBQKBNR w KQkq - 0 3")
        testTakeBack(expect: "rnbqkbnr/p1pppppp/8/1p6/P6P/8/1PPPPPP1/RNBQKBNR b KQkq h3 0 2")
        testTakeBack(expect: "rnbqkbnr/p1pppppp/8/1p6/P7/8/1PPPPPPP/RNBQKBNR w KQkq b6 0 2")
        testTakeBack(expect: "rnbqkbnr/pppppppp/8/8/P7/8/1PPPPPPP/RNBQKBNR b KQkq a3 0 1")
        testTakeBack(expect: Constants.startingFEN)
//
//        testForward(expect: "rnbqkbnr/p1pppppp/8/1p6/P7/8/1PPPPPPP/RNBQKBNR w KQkq b6 0 2")
//        testForward(expect: "rnbqkbnr/p1pppppp/8/8/Pp5P/8/1PPPPPP1/RNBQKBNR w KQkq - 0 3")
//        testForward(expect: "rnbqkbnr/p1pppppp/8/8/P6P/R1p5/1P1PPPP1/1NBQKBNR b Kkq - 1 4")
//        testForward(expect: "rnbqkbnr/p1pppppp/8/8/P6P/R7/1p1PPPP1/1NBQKBNR w Kkq - 0 5")

    }
    
//    private func testForward(expect: String) {
//        self.game.forwardMove(completion: {
//            XCTAssertEqual(self.game.currFEN, expect, "the calculated forward FEN is not equal to what is expected")
//        })
//    }
//
    private func testTakeBack(expect: String) {
        self.game.moveBack()
        XCTAssertEqual(self.game.fen.string, expect, "the calculated backward FEN is not equal to what is expected")
//        self.game.undoMove(completion: {
//            XCTAssertEqual(self.game.currFEN, expect, "the calculated backward FEN is not equal to what is expected")
//        })
    }
    
    private func testSingleMove(from: Coordinate, to: Coordinate, enpassantCoord: Coordinate? = nil,expect: String) {
//        let piece = self.game.pieceMap[from]
//        XCTAssertTrue(piece != nil, "Couldn't find the piece you were looking for")
        
//        self.game.moveMade(piece: piece!, to: to, enpassantTakeCoord: enpassantCoord)
//        XCTAssertEqual(self.game.currFEN, expect, "the calculated FEN is not equal to what is expected")
        self.game.move(from: from, to: to)
        XCTAssertEqual(self.game.fen.string, expect, "the calculated FEN is not equal to what is expected")
    }
    
    /*
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    */
}
