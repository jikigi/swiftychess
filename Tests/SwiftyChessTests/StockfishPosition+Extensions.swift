//
//  StockfishPosition+Extensions.swift
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 8/11/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import Foundation
import SwiftyChess
import StockfishChess

extension StockfishPosition {
    
    func possibleMoves(from coordinate: Coordinate) -> UInt64 {
        return self.destinationSquares(from: Square(rawValue: UInt(coordinate.intVal))!)
            .reduce(into: UInt64(0), {
                $0 |= (1 << $1.intValue)
            })
    }
}
