//
//  ChessOpeningsTests.swift
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 10/21/17.
//  Copyright © 2017 Casey Evanish. All rights reserved.
//

import XCTest
@testable import SwiftyChess

class PGKeyTests: XCTestCase {
    
    var game: Game!
    
    override func setUp() {
        super.setUp()
        self.game = Game()
//        self.game = Game(playAs: .white)
//        self.game.initPieceMap()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPG1() {

        //moving white pawn from e2 to e4
        testSingleMove(from: Coordinate(file: .FILE_E, rank: .RANK_2), to: Coordinate(file: .FILE_E, rank: .RANK_4), expect: 0x823c9b50fd114196)
        
        //moving black pawn from d7 to d5
        testSingleMove(from: Coordinate(file: .FILE_D, rank: .RANK_7), to: Coordinate(file: .FILE_D, rank: .RANK_5), expect: 0x0756b94461c50fb0)
        
        //moving white pawn from e4 to e5
        testSingleMove(from: Coordinate(file: .FILE_E, rank: .RANK_4), to: Coordinate(file: .FILE_E, rank: .RANK_5), expect: 0x662fafb965db29d4)
        
        //moving black pawn from f7 to f5
        testSingleMove(from: Coordinate(file: .FILE_F, rank: .RANK_7), to: Coordinate(file: .FILE_F, rank: .RANK_5), expect: 0x22a48b5a8e47ff78)
        
        //moving white king from e1 to e2
        testSingleMove(from: Coordinate(file: .FILE_E, rank: .RANK_1), to: Coordinate(file: .FILE_E, rank: .RANK_2), expect: 0x652a607ca3f242c1)
        
        //moving black king from e8 to f7
        testSingleMove(from: Coordinate(file: .FILE_E, rank: .RANK_8), to: Coordinate(file: .FILE_F, rank: .RANK_7), expect: 0x00fdd303c946bdd9)
        
        testTakeBack(expect: 0x652a607ca3f242c1)
        testTakeBack(expect: 0x22a48b5a8e47ff78)
        testTakeBack(expect: 0x662fafb965db29d4)
        testTakeBack(expect: 0x0756b94461c50fb0)
        testTakeBack(expect: 0x823c9b50fd114196)
        testTakeBack(expect: Constants.startingPGKey)
//
//        testForward(expect: 0x0756b94461c50fb0)
//        testForward(expect: 0x22a48b5a8e47ff78)
//        testForward(expect: 0x00fdd303c946bdd9)
        
    }
    
    func testPG2() {

        //moving white pawn from a2 to a4
        testSingleMove(from: Coordinate(file: .FILE_A, rank: .RANK_2), to: Coordinate(file: .FILE_A, rank: .RANK_4), expect: 0x2df2e8f47b022952)

        //moving black pawn from b7 to b5
        testSingleMove(from: Coordinate(file: .FILE_B, rank: .RANK_7), to: Coordinate(file: .FILE_B, rank: .RANK_5), expect: 0x4df682e1e0af946f)

        //moving white pawn from h2 to h4
        testSingleMove(from: Coordinate(file: .FILE_H, rank: .RANK_2), to: Coordinate(file: .FILE_H, rank: .RANK_4), expect: 0xd1551ec84b90ed11)

        //moving black pawn from b5 to b4
        testSingleMove(from: Coordinate(file: .FILE_B, rank: .RANK_5), to: Coordinate(file: .FILE_B, rank: .RANK_4), expect: 0xb0982f168a89b452)

        //moving white pawn from c2 to c4
        testSingleMove(from: Coordinate(file: .FILE_C, rank: .RANK_2), to: Coordinate(file: .FILE_C, rank: .RANK_4), expect: 0x3c8123ea7b067637)

        //moving black pawn from b4 to c3
        testSingleMove(from: Coordinate(file: .FILE_B, rank: .RANK_4), to: Coordinate(file: .FILE_C, rank: .RANK_3), enpassantCoord: Coordinate(file: .FILE_C, rank: .RANK_4), expect: 0x93d32682782edfae)

        //moving white rook from a1 to a3
        testSingleMove(from: Coordinate(file: .FILE_A, rank: .RANK_1), to: Coordinate(file: .FILE_A, rank: .RANK_3), expect: 0x5c3f9b829b279560)

        //moving black pawn from c3 to b2
        testSingleMove(from: Coordinate(file: .FILE_C, rank: .RANK_3), to: Coordinate(file: .FILE_B, rank: .RANK_2), expect: 0x3bed884bb66ebcda)

        testTakeBack(expect: 0x5c3f9b829b279560)
        testTakeBack(expect: 0x93d32682782edfae)
        testTakeBack(expect: 0x3c8123ea7b067637)
        testTakeBack(expect: 0xb0982f168a89b452)
        testTakeBack(expect: 0xd1551ec84b90ed11)
        testTakeBack(expect: 0x4df682e1e0af946f)
        testTakeBack(expect: 0x2df2e8f47b022952)
        testTakeBack(expect: Constants.startingPGKey)
//
//        testForward(expect: 0x4df682e1e0af946f)
//        testForward(expect: 0xb0982f168a89b452)
//        testForward(expect: 0x93d32682782edfae)
//        testForward(expect: 0x3bed884bb66ebcda)
    }

//    private func testForward(expect: UInt64) {
//        self.game.forwardMove(completion: {
//            XCTAssertEqual(self.game.currPGKey, expect, "the calculated pgKey is not equal to what is expected")
//        })
//    }
//
    private func testTakeBack(expect: UInt64) {
        self.game.moveBack()
        XCTAssertEqual(self.game.polyglot.key, expect, "the calculated pgKey is not equal to what is expected")
//        self.game.undoMove(completion: {
//            XCTAssertEqual(self.game.currPGKey, expect, "the calculated pgKey is not equal to what is expected")
//        })
    }
    
    private func testSingleMove(from: Coordinate, to: Coordinate, enpassantCoord: Coordinate? = nil, expect: UInt64) {
//        let piece = self.game.pieceMap[from]
//        XCTAssertTrue(piece != nil, "Couldn't find the piece you were looking for")

//        self.game.moveMade(piece: piece!, to: to, enpassantTakeCoord: enpassantCoord)
        self.game.move(from: from, to: to)
//        XCTAssertEqual(self.game.currPGKey, expect, "the calculated pgKey is not equal to what is expected")
        XCTAssertEqual(self.game.polyglot.key, expect, "the calculated pgKey is not equal to what is expected")
    }
    
    /*
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    */
    
}
