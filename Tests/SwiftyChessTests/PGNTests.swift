//
//  PGNTests.swift
//  SwiftyChess_Tests
//
//  Created by Casey Evanish on 7/11/20.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import XCTest
import SwiftyChess

class PGNTests: XCTestCase {

    func testPGNTagsAlekhine() throws {
        var directoryCopy = TestPGNs.directory
        directoryCopy.appendPathComponent("Rodzinski_vs_Alekhine Alexander A _1913.__.__.pgn")
        let pgn = try XCTUnwrap(PGN(url: directoryCopy), "the pgn should exist and parsing should be successful")
        
        let event = try XCTUnwrap(pgn.event)
        XCTAssertEqual(event, "Rodzinski - Alekhine Alexander A (RUS) (1913.??.??)")
        let site = try XCTUnwrap(pgn.site)
        XCTAssertEqual(site, "Paris (France)")
        let date = try XCTUnwrap(pgn.date)
        XCTAssertEqual(date, "1913.??.??")
        XCTAssertNil(pgn.round)
        let white = try XCTUnwrap(pgn.white)
        XCTAssertEqual(white, "Rodzinski")
        let black = try XCTUnwrap(pgn.black)
        XCTAssertEqual(black, "Alekhine Alexander A ")
        let result = try XCTUnwrap(pgn.result)
        XCTAssertEqual(result, .black)
        XCTAssertNil(pgn.eco)
        XCTAssertNil(pgn.whiteElo)
        XCTAssertNil(pgn.blackElo)
    }
    
    func testPGNTagskcboy66() throws {
        var directoryCopy = TestPGNs.directory
        directoryCopy.appendPathComponent("tzograf_vs_kcboy66_2019.08.09.pgn")
        let pgn = try XCTUnwrap(PGN(url: directoryCopy),  "the pgn should exist and parsing should be successful")
        
        let event = try XCTUnwrap(pgn.event)
        XCTAssertEqual(event, "Live Chess")
        let site = try XCTUnwrap(pgn.site)
        XCTAssertEqual(site, "Chess.com")
        let date = try XCTUnwrap(pgn.date)
        XCTAssertEqual(date, "2019.08.09")
        XCTAssertNil(pgn.round)
        let white = try XCTUnwrap(pgn.white)
        XCTAssertEqual(white, "tzograf")
        let black = try XCTUnwrap(pgn.black)
        XCTAssertEqual(black, "kcboy66")
        let result = try XCTUnwrap(pgn.result)
        XCTAssertEqual(result, .black)
        let whiteElo = try XCTUnwrap(pgn.whiteElo)
        XCTAssertEqual(whiteElo, 1660)
        let blackElo = try XCTUnwrap(pgn.blackElo)
        XCTAssertEqual(blackElo, 1711)
        XCTAssertNil(pgn.eco)
    }

}
