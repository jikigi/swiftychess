//
//  StockfishPGNTests.swift
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 8/11/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import XCTest
import StockfishChess
@testable import SwiftyChess

class StockfishPGNTests: XCTestCase {
    
    func testAllPGNs() {
        guard let enumerator = TestPGNs.enumerator else {
            XCTFail("cannot find pgn enumerator")
            return
        }
        let game = Game()
//        while let nextPath = enumerator.nextObject() as? String {
//            var directoryCopy = TestPGNs.directory
//            directoryCopy.appendPathComponent(nextPath)
            let nextPath = "Barua, D vs Speelman, J._1996.pgn"
            var directoryCopy = Bundle.module.resourceURL!
            directoryCopy.appendPathComponent(nextPath)
            guard directoryCopy.pathExtension == "pgn", let pgn = PGN(url: directoryCopy) else { fatalError() }
            print("testing \(#function) for \(nextPath)")
            let stockfishPosition = StockfishPosition(fen: Constants.startingFEN)
            game.reset()
            print("--forward--")
            for san in pgn.notations {
                guard let (fromCoord, toCoord, promotion) = game.moveFromNotation(notation: san),
                    let nextMove = game.move(from: fromCoord, to: toCoord, promotion: promotion) else {
                        XCTFail("All moves from the pgn files should be valid")
                        return
                }
                var promotionType: PromotionType = .NO_PIECE_TYPE
                
                if case let .promotion(selection, _, _, _) = nextMove.type {
                    switch selection {
                    case .bishop:
                        promotionType = .BISHOP
                    case .knight:
                        promotionType = .KNIGHT
                    case .rook:
                        promotionType = .ROOK
                    case .queen:
                        promotionType = .QUEEN
                    }
                }
                stockfishPosition.doMove(from: Square(rawValue: UInt(nextMove.from.intVal))!,
                                         to: Square(rawValue: UInt(nextMove.to.intVal))!,
                                         promotion: promotionType)

                helpTestPositionAgainstStockfish(game: game,
                                                 stockfishPosition: stockfishPosition,
                                                 gameName: nextPath)
            }
//            })
            // now go back and test the positions again
            print("--backward--")
            while game.canGoBack() {
                game.moveBack()
                stockfishPosition.takeBack()
                helpTestPositionAgainstStockfish(game: game,
                                                 stockfishPosition: stockfishPosition,
                                                 gameName: nextPath)
            }
//        }
    }
    
    private func calculateTime(block : (() -> Void)) {
        let start = DispatchTime.now()
        block()
        let end = DispatchTime.now()
        let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
        let timeInterval = Double(nanoTime) / 1_000_000_000
        print("Time: \(timeInterval) seconds")
    }
    
    private func helpTestPositionAgainstStockfish(game: Game, stockfishPosition: StockfishPosition, gameName: String) {
        Coordinate
            .allCases
            .compactMap({ coordinate -> Coordinate? in
                guard let piece = game.piece(at: coordinate), piece.color == game.turn else {
                    return nil
                }
                return coordinate
            })
            .forEach({ coordinate in
                let stockFishMoves = stockfishPosition.possibleMoves(from: coordinate)
                let myCodeMoves = game.legalMoves(forPieceAt: coordinate)
                
                XCTAssertEqual(stockFishMoves, myCodeMoves, "the possible moves of a \(type(of: game.piece(at: coordinate)!)) on \(coordinate) is wrong in move \(game.getCurrentChessMoveNumber()) for game \(gameName). Stockfish says \(stockFishMoves.toCoordinateArray()) while your code says \(myCodeMoves.toCoordinateArray())")
            })
    }
    
}
