//
//  PinnedTests.swift
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 11/28/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import XCTest
@testable import SwiftyChess

class PinnedTests: XCTestCase {

    var game: Game!
    
    override func setUp() {
        self.game = Game()
    }

    func testPinnedKnight() {
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_2),
                       to: Coordinate(file: .FILE_D, rank: .RANK_4))
        
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_7),
                       to: Coordinate(file: .FILE_E, rank: .RANK_6))
        
        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_1),
                       to: Coordinate(file: .FILE_C, rank: .RANK_3))
        
        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_8),
                       to: Coordinate(file: .FILE_B, rank: .RANK_4))

        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_C, rank: .RANK_3)),
                       0,
                       "The knight should be pinned and have no possible moves")
        
        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_2),
                       to: Coordinate(file: .FILE_A, rank: .RANK_3))
        
        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_4),
                       to: Coordinate(file: .FILE_A, rank: .RANK_5))
        
        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_C, rank: .RANK_3)),
                       0,
                       "The knight should still be pinned")
        
        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_2),
                       to: Coordinate(file: .FILE_B, rank: .RANK_4))

        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_5),
                       to: Coordinate(file: .FILE_B, rank: .RANK_6))

        XCTAssertNotEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_C, rank: .RANK_3)),
                          0,
                          "The knight should be unpinned")
    }
    
    func testPinnedBishop() {
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_2),
                       to: Coordinate(file: .FILE_D, rank: .RANK_4))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_7),
                       to: Coordinate(file: .FILE_E, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_C, rank: .RANK_1),
                       to: Coordinate(file: .FILE_D, rank: .RANK_2))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_8),
                       to: Coordinate(file: .FILE_B, rank: .RANK_4))

        let correctBishopPossibleMoves =
            Coordinate(file: .FILE_B, rank: .RANK_4).uint64Val |
            Coordinate(file: .FILE_C, rank: .RANK_3).uint64Val

        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_D, rank: .RANK_2)),
                       correctBishopPossibleMoves, "The bishop should be pinned with B4 and C3 as possible moves")

        self.game.move(from: Coordinate(file: .FILE_C, rank: .RANK_2),
                       to: Coordinate(file: .FILE_C, rank: .RANK_3))

        // random move
        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_7),
                       to: Coordinate(file: .FILE_A, rank: .RANK_6))

        XCTAssertTrue(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_D, rank: .RANK_2)).contains(Coordinate(file: .FILE_E, rank: .RANK_3).uint64Val),
                      "The bishop should be unpinned")

        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_2),
                       to: Coordinate(file: .FILE_E, rank: .RANK_2))

        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_7),
                       to: Coordinate(file: .FILE_H, rank: .RANK_6))

        let correctPawnPossibleMoves = Coordinate(file: .FILE_B, rank: .RANK_4).uint64Val
        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_C, rank: .RANK_3)),
                       correctPawnPossibleMoves,
                       "The pawn should only be able to move to B4")

        // random move
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_2),
                       to: Coordinate(file: .FILE_H, rank: .RANK_3))
        
        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_4),
                       to: Coordinate(file: .FILE_A, rank: .RANK_5))

        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_C, rank: .RANK_3)),
                       0,
                       "The pawn should now not be able to move anywhere")
    }

    func testUnpinWhenKingMoves() {
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_2),
                       to: Coordinate(file: .FILE_D, rank: .RANK_4))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_7),
                       to: Coordinate(file: .FILE_E, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_1),
                       to: Coordinate(file: .FILE_C, rank: .RANK_3))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_8),
                       to: Coordinate(file: .FILE_B, rank: .RANK_4))

        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_C, rank: .RANK_3)),
                       0,
                       "The knight should be pinned and have no possible moves")

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_2),
                       to: Coordinate(file: .FILE_F, rank: .RANK_3))

        // random move
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_7),
                       to: Coordinate(file: .FILE_H, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_1),
                       to: Coordinate(file: .FILE_F, rank: .RANK_2))

        XCTAssertNotEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_C, rank: .RANK_3)),
                          0,
                          "The knight should be unpinned")
    }

    func testDoublePin() {
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_2),
                       to: Coordinate(file: .FILE_D, rank: .RANK_4))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_7),
                       to: Coordinate(file: .FILE_E, rank: .RANK_5))

        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_1),
                       to: Coordinate(file: .FILE_C, rank: .RANK_3))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_5),
                       to: Coordinate(file: .FILE_D, rank: .RANK_4))

        // random move
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_2),
                       to: Coordinate(file: .FILE_H, rank: .RANK_3))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_8),
                       to: Coordinate(file: .FILE_B, rank: .RANK_4))

        // random move
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_3),
                       to: Coordinate(file: .FILE_H, rank: .RANK_4))
        
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_8),
                       to: Coordinate(file: .FILE_E, rank: .RANK_7))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_2),
                       to: Coordinate(file: .FILE_E, rank: .RANK_4))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_7),
                       to: Coordinate(file: .FILE_F, rank: .RANK_5))

        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_C, rank: .RANK_3)),
                       0,
                       "The knight should be pinned and have no possible moves")

        let correctWhiteE4PawnMoves = Coordinate(file: .FILE_E, rank: .RANK_5).uint64Val
        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_E, rank: .RANK_4)),
                       correctWhiteE4PawnMoves,
                       "White E4 pawn should only be able to move forward")
    }

}
