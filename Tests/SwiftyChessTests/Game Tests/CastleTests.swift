//
//  CastleTests.swift
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 11/26/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import XCTest
@testable import SwiftyChess

class CastleTests: XCTestCase {

    var game: Game!
    
    override func setUp() {
        self.game = Game()
    }

    func testInitialCastleRights() {
        XCTAssertTrue(self.game.castleRights[.white, .queen], "should be able to castle queenside on white when first starting the game")
        XCTAssertTrue(self.game.castleRights[.white, .king], "should be able to castle kingside on white when first starting the game")
        XCTAssertTrue(self.game.castleRights[.black, .queen], "should be able to castle queenside on black when first starting the game")
        XCTAssertTrue(self.game.castleRights[.black, .king], "should be able to castle kingside on black when first starting the game")
    }
    
    func testMoveQueenSideRooks() {
        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_2),
                       to: Coordinate(file: .FILE_A, rank: .RANK_4))
        
        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_7),
                       to: Coordinate(file: .FILE_A, rank: .RANK_5))
        
        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_1),
                       to: Coordinate(file: .FILE_A, rank: .RANK_2))
        
        XCTAssertFalse(self.game.castleRights[.white, .queen], "White should not be able to castle queenside after queenside rook moves")
        
        XCTAssertTrue(self.game.castleRights[.white, .king], "White should still be able to castle kingside")
        XCTAssertTrue(self.game.castleRights[.black, .queen], "Black should still be able to castle queenside")
        XCTAssertTrue(self.game.castleRights[.black, .king], "Black should still be able to castle kingside")
        
        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_8),
                       to: Coordinate(file: .FILE_A, rank: .RANK_7))
        
        XCTAssertFalse(self.game.castleRights[.black, .queen], "Black should not be able to castle queenside after queenside rook moves")
        XCTAssertTrue(self.game.castleRights[.black, .king], "Black should still be ablt to castle kingside")
        
        XCTAssertFalse(self.game.castleRights[.white, .queen], "White should still not be able to castle queenside")
        XCTAssertTrue(self.game.castleRights[.white, .king], "White should be able to castle kingside")
    }
    
    func testMoveKingSideRooks() {
        
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_2),
                       to: Coordinate(file: .FILE_H, rank: .RANK_4))
        
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_7),
                       to: Coordinate(file: .FILE_H, rank: .RANK_5))
        
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_1),
                       to: Coordinate(file: .FILE_H, rank: .RANK_2))
        
        XCTAssertFalse(self.game.castleRights[.white, .king], "White should not be able to castle kingside after kingside rook moves")
        XCTAssertTrue(self.game.castleRights[.white, .queen], "White should still be able to castle queenside")
        
        XCTAssertTrue(self.game.castleRights[.black, .king], "Black should still be able to castle kingside")
        XCTAssertTrue(self.game.castleRights[.black, .queen], "Black should still be able to castle queenside")
        
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_8),
                       to: Coordinate(file: .FILE_H, rank: .RANK_7))
        
        XCTAssertFalse(self.game.castleRights[.black, .king], "Black should not be able to castle kingside after kingside rook moves")
        XCTAssertTrue(self.game.castleRights[.black, .queen], "Black should still be able to castle queenside")
        
        XCTAssertFalse(self.game.castleRights[.white, .king], "White should still not be able to castle kingside")
        XCTAssertTrue(self.game.castleRights[.white, .queen], "White should still be able to castle queenside")
    }
    
    func testMoveKings() {
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_2),
                       to: Coordinate(file: .FILE_E, rank: .RANK_4))
        
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_7),
                       to: Coordinate(file: .FILE_E, rank: .RANK_5))
        
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_1),
                       to: Coordinate(file: .FILE_E, rank: .RANK_2))
        
        XCTAssertFalse(self.game.castleRights[.white, .king], "white should not be able to castle kingside after moving the king")
        XCTAssertFalse(self.game.castleRights[.white, .queen], "white should not be able to castle queenside after moving the king")
        XCTAssertTrue(self.game.castleRights[.black, .queen], "Black should still be able to castle queenside")
        XCTAssertTrue(self.game.castleRights[.black, .king], "Black should still be able to castle kingside")
        
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_8),
                       to: Coordinate(file: .FILE_E, rank: .RANK_7))
        
        XCTAssertFalse(self.game.castleRights[.white, .king], "white should still not be able to castle kingside after moving the king")
        XCTAssertFalse(self.game.castleRights[.white, .queen], "white should still not be able to castle queenside after moving the king")
        
        XCTAssertFalse(self.game.castleRights[.black, .king], "black should not be able to castle kingside after moving the king")
        XCTAssertFalse(self.game.castleRights[.black, .queen], "black should not be able to castle queenside after moving the king")
    }
    
    func testCastleQueenside() {
        let whiteKing = self.game.piece(at: Coordinate(file: .FILE_E, rank: .RANK_1)) as! King
        let blackKing = self.game.piece(at: Coordinate(file: .FILE_E, rank: .RANK_8)) as! King
        
        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_1), to: Coordinate(file: .FILE_C, rank: .RANK_3))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))
        XCTAssertFalse(queensideCastleAvailable(king: blackKing))
        
        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_8), to: Coordinate(file: .FILE_C, rank: .RANK_6))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))
        XCTAssertFalse(queensideCastleAvailable(king: blackKing))
        
        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_2), to: Coordinate(file: .FILE_B, rank: .RANK_3))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))
        XCTAssertFalse(queensideCastleAvailable(king: blackKing))
        
        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_7), to: Coordinate(file: .FILE_B, rank: .RANK_6))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))
        XCTAssertFalse(queensideCastleAvailable(king: blackKing))
        
        self.game.move(from: Coordinate(file: .FILE_C, rank: .RANK_1), to: Coordinate(file: .FILE_B, rank: .RANK_2))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))
        XCTAssertFalse(queensideCastleAvailable(king: blackKing))
        
        self.game.move(from: Coordinate(file: .FILE_C, rank: .RANK_8), to: Coordinate(file: .FILE_B, rank: .RANK_7))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))
        XCTAssertFalse(queensideCastleAvailable(king: blackKing))
        
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_2), to: Coordinate(file: .FILE_D, rank: .RANK_3))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))
        XCTAssertFalse(queensideCastleAvailable(king: blackKing))
        
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_7), to: Coordinate(file: .FILE_D, rank: .RANK_6))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))
        XCTAssertFalse(queensideCastleAvailable(king: blackKing))
        
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_1), to: Coordinate(file: .FILE_D, rank: .RANK_6))
        XCTAssertTrue(queensideCastleAvailable(king: whiteKing))
        XCTAssertFalse(queensideCastleAvailable(king: blackKing))
        
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_8), to: Coordinate(file: .FILE_D, rank: .RANK_7))
        XCTAssertTrue(queensideCastleAvailable(king: whiteKing))
        XCTAssertTrue(queensideCastleAvailable(king: blackKing))
        
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_1), to: Coordinate(file: .FILE_C, rank: .RANK_1))
        //self.game.moveMade(piece: whiteKing, to: Coordinate(file: .FILE_C, rank: .RANK_1))
        XCTAssertFalse(self.game.castleRights[.white, .king], "white should not be able to castle kingside after castling queenside")
        XCTAssertFalse(self.game.castleRights[.white, .queen], "white should not be able to castle queenside after castling queenside")
        XCTAssertTrue(self.game.castleRights[.black, .king], "black should be able to castle kingside")
        XCTAssertTrue(self.game.castleRights[.black, .queen], "black should be able to castle queenside")
        
        let whiteRookCoordBeforeCastling = Coordinate(file: .FILE_A, rank: .RANK_1)
        let whiteRookCoordAfterCastling = Coordinate(file: .FILE_D, rank: .RANK_1)
        guard let whiteRookAfterMoving = self.game.piece(at: whiteRookCoordAfterCastling) else {
            XCTFail("There should be a piece at D1")
            return
        }
        XCTAssertNil(self.game.piece(at: whiteRookCoordBeforeCastling), "There should not be a piece at A1 after castling queenside")
        XCTAssertTrue(whiteRookAfterMoving is Rook, "The piece at D1 should be a rook")
        XCTAssertTrue(whiteRookAfterMoving.color == .white, "The rook should be white")
        
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_8), to: Coordinate(file: .FILE_C, rank: .RANK_8))
        XCTAssertFalse(self.game.castleRights[.white, .king], "white should not be able to castle kingside after castling queenside")
        XCTAssertFalse(self.game.castleRights[.white, .queen], "white should not be able to castle queenside after castling queenside")
        XCTAssertFalse(self.game.castleRights[.black, .king], "black should not be able to castle kingside after castling queenside")
        XCTAssertFalse(self.game.castleRights[.black, .queen], "black should not be able to castle queenside after castling queenside")
        
        let blackRookCoordBeforeCastling = Coordinate(file: .FILE_A, rank: .RANK_8)
        let blackRookCoordAfterCastling = Coordinate(file: .FILE_D, rank: .RANK_8)

        guard let blackRookAfterMoving = self.game.piece(at: blackRookCoordAfterCastling) else {
            XCTFail("There should be a piece at D8")
            return
        }
        
        XCTAssertNil(self.game.piece(at: blackRookCoordBeforeCastling), "There should not be a piece at A8 after castling queenside")
        XCTAssertTrue(blackRookAfterMoving is Rook, "The piece at D8 should be a rook")
        XCTAssertTrue(blackRookAfterMoving.color == .black, "The rook should be black")
    }
    
    func testCastleKingside() {

        let whiteKing = self.game.piece(at: Coordinate(file: .FILE_E, rank: .RANK_1)) as! King
        let blackKing = self.game.piece(at: Coordinate(file: .FILE_E, rank: .RANK_8)) as! King

        self.game.move(from: Coordinate(file: .FILE_G, rank: .RANK_1),
                       to: Coordinate(file: .FILE_F, rank: .RANK_3))
        XCTAssertFalse(kingsideCastleAvailable(king: whiteKing))
        XCTAssertFalse(kingsideCastleAvailable(king: blackKing))

        self.game.move(from: Coordinate(file: .FILE_G, rank: .RANK_8),
                       to: Coordinate(file: .FILE_F, rank: .RANK_6))
        XCTAssertFalse(kingsideCastleAvailable(king: whiteKing))
        XCTAssertFalse(kingsideCastleAvailable(king: blackKing))

        self.game.move(from: Coordinate(file: .FILE_G, rank: .RANK_2),
                       to: Coordinate(file: .FILE_G, rank: .RANK_3))
        XCTAssertFalse(kingsideCastleAvailable(king: whiteKing))
        XCTAssertFalse(kingsideCastleAvailable(king: blackKing))

        self.game.move(from: Coordinate(file: .FILE_G, rank: .RANK_7),
                       to: Coordinate(file: .FILE_G, rank: .RANK_6))
        XCTAssertFalse(kingsideCastleAvailable(king: whiteKing))
        XCTAssertFalse(kingsideCastleAvailable(king: blackKing))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_1),
                       to: Coordinate(file: .FILE_G, rank: .RANK_2))
        XCTAssertTrue(kingsideCastleAvailable(king: whiteKing))
        XCTAssertFalse(kingsideCastleAvailable(king: blackKing))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_8),
                       to: Coordinate(file: .FILE_G, rank: .RANK_7))
        XCTAssertTrue(kingsideCastleAvailable(king: whiteKing))
        XCTAssertTrue(kingsideCastleAvailable(king: blackKing))

        let whiteRookCoordAfterCastling = Coordinate(file: .FILE_F, rank: .RANK_1)
        
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_1),
                       to: Coordinate(file: .FILE_F, rank: .RANK_1))
        guard let whiteRookAfterMoving = self.game.piece(at: whiteRookCoordAfterCastling) else {
            XCTFail("There should be a piece at F1")
            return
        }
        
        XCTAssertNil(self.game.piece(at: Coordinate(file: .FILE_H, rank: .RANK_1)), "There should not be a piece at H1 after castling")
        XCTAssertTrue(whiteRookAfterMoving is Rook, "The piece at F1 should be a rook")
        XCTAssertTrue(whiteRookAfterMoving.color == .white, "The rook should be white")

        let blackRookCoordAfterCastling = Coordinate(file: .FILE_F, rank: .RANK_8)
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_8), to: Coordinate(file: .FILE_G, rank: .RANK_8))
        
        guard let blackRookAfterMoving = self.game.piece(at: blackRookCoordAfterCastling) else {
            XCTFail("There should be a piece at F8")
            return
        }
        
        XCTAssertNil(self.game.piece(at: Coordinate(file: .FILE_H, rank: .RANK_8)), "There should not be a piece at H8 after castling")
        XCTAssertTrue(blackRookAfterMoving is Rook, "The piece at F1 should be a rook")
        XCTAssertTrue(blackRookAfterMoving.color == .black, "The rook should be white")
    }

    // when the opposing piece is attacking the castling path
    func testInterceptKingsideCastling() {
        let whiteKing = self.game.piece(at: Coordinate(file: .FILE_E, rank: .RANK_1)) as! King

        self.game.move(from: Coordinate(file: .FILE_G, rank: .RANK_1),
                       to: Coordinate(file: .FILE_F, rank: .RANK_3))
        XCTAssertFalse(kingsideCastleAvailable(king: whiteKing))

        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_7),
                       to: Coordinate(file: .FILE_B, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_G, rank: .RANK_2),
                       to: Coordinate(file: .FILE_G, rank: .RANK_3))
        XCTAssertFalse(kingsideCastleAvailable(king: whiteKing))

        self.game.move(from: Coordinate(file: .FILE_C, rank: .RANK_8),
                       to: Coordinate(file: .FILE_A, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_1),
                       to: Coordinate(file: .FILE_G, rank: .RANK_2))
        XCTAssertTrue(kingsideCastleAvailable(king: whiteKing))

        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_7),
                       to: Coordinate(file: .FILE_D, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_2),
                       to: Coordinate(file: .FILE_E, rank: .RANK_3))
        XCTAssertFalse(kingsideCastleAvailable(king: whiteKing))
    }

    // when the opposing piece is attacking the castling path
    func testInterceptQueensideCastling() {
        let whiteKing = self.game.piece(at: Coordinate(file: .FILE_E, rank: .RANK_1)) as! King

        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_1),
                       to: Coordinate(file: .FILE_C, rank: .RANK_3))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))

        self.game.move(from: Coordinate(file: .FILE_G, rank: .RANK_7),
                       to: Coordinate(file: .FILE_G, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_2),
                       to: Coordinate(file: .FILE_B, rank: .RANK_3))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_8),
                       to: Coordinate(file: .FILE_H, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_C, rank: .RANK_1),
                       to: Coordinate(file: .FILE_B, rank: .RANK_2))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_7),
                       to: Coordinate(file: .FILE_E, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_2),
                       to: Coordinate(file: .FILE_D, rank: .RANK_3))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_6),
                       to: Coordinate(file: .FILE_E, rank: .RANK_5))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_2),
                       to: Coordinate(file: .FILE_E, rank: .RANK_4))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))

        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_7),
                       to: Coordinate(file: .FILE_A, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_1),
                       to: Coordinate(file: .FILE_G, rank: .RANK_4))
        XCTAssertFalse(queensideCastleAvailable(king: whiteKing))

        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_6),
                       to: Coordinate(file: .FILE_A, rank: .RANK_5))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_2),
                       to: Coordinate(file: .FILE_F, rank: .RANK_4))
        XCTAssertTrue(queensideCastleAvailable(king: whiteKing))
    }

    func testCantCastleKingsideOnCheck() {
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_2),
                       to: Coordinate(file: .FILE_D, rank: .RANK_3))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_7),
                       to: Coordinate(file: .FILE_E, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_G, rank: .RANK_1),
                       to: Coordinate(file: .FILE_F, rank: .RANK_3))

        // random move
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_7),
                       to: Coordinate(file: .FILE_H, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_2),
                       to: Coordinate(file: .FILE_E, rank: .RANK_3))

        // random move
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_6),
                       to: Coordinate(file: .FILE_H, rank: .RANK_5))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_1),
                       to: Coordinate(file: .FILE_E, rank: .RANK_2))

        let whiteKing = self.game.piece(at: Coordinate(file: .FILE_E, rank: .RANK_1)) as! King
        XCTAssertTrue(kingsideCastleAvailable(king: whiteKing), "white should be able to castle kingside")

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_8),
                       to: Coordinate(file: .FILE_B, rank: .RANK_4))

        XCTAssertTrue(self.game.isInCheck, "The game should be in the check state")

        XCTAssertFalse(kingsideCastleAvailable(king: whiteKing), "white should not be able to castle kingside when in check")
    }

    func testCantCastleQueenSideOnCheck() {
        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_2),
                       to: Coordinate(file: .FILE_F, rank: .RANK_3))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_7),
                       to: Coordinate(file: .FILE_E, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_1),
                       to: Coordinate(file: .FILE_C, rank: .RANK_3))

        //random move
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_7),
                       to: Coordinate(file: .FILE_H, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_2),
                       to: Coordinate(file: .FILE_D, rank: .RANK_3))

        //random move
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_6),
                       to: Coordinate(file: .FILE_H, rank: .RANK_5))

        self.game.move(from: Coordinate(file: .FILE_C, rank: .RANK_1),
                       to: Coordinate(file: .FILE_E, rank: .RANK_3))

        //random move
        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_7),
                       to: Coordinate(file: .FILE_A, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_1),
                       to: Coordinate(file: .FILE_D, rank: .RANK_2))

        let whiteKing = self.game.piece(at: Coordinate(file: .FILE_E, rank: .RANK_1)) as! King
        XCTAssertTrue(queensideCastleAvailable(king: whiteKing), "white king should be able to castle queenside")

        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_8),
                       to: Coordinate(file: .FILE_H, rank: .RANK_4))

        XCTAssertFalse(queensideCastleAvailable(king: whiteKing), "white king should not be able to castle queenside when in check")
    }
    
    private func queensideCastleAvailable(king: King) -> Bool {
        let queenSideCastleCoordinate = king.color == .white ?
            Coordinate(file: .FILE_C, rank: .RANK_1) :
            Coordinate(file: .FILE_C, rank: .RANK_8)
        
        let kingCoord = king.color == .white ?
            Coordinate(file: .FILE_E, rank: .RANK_1) :
            Coordinate(file: .FILE_E, rank: .RANK_8)
        
        return self.game.legalMoves(forPieceAt: kingCoord).contains(queenSideCastleCoordinate.uint64Val)
    }
    
    private func kingsideCastleAvailable(king: King) -> Bool {
        let kingSideCastleCoordinate = king.color == .white ?
            Coordinate(file: .FILE_G, rank: .RANK_1) :
            Coordinate(file: .FILE_G, rank: .RANK_8)
        
        let kingCoord = king.color == .white ?
            Coordinate(file: .FILE_E, rank: .RANK_1) :
            Coordinate(file: .FILE_E, rank: .RANK_8)
        
        return self.game.legalMoves(forPieceAt: kingCoord).contains(kingSideCastleCoordinate.uint64Val)
    }

}
