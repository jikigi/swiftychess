//
//  GameResetTests.swift
//  SwiftyChess_Tests
//
//  Created by Casey Evanish on 7/15/20.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import XCTest
@testable import SwiftyChess

class GameMoveCountTests: XCTestCase {

    let game = Game()
    
    func testNumberOfIndividualMoves() {
        let pgnString = "1. e4 e5 2. Nf3 d6 3. Ng1"
        let pgn = PGN(string: pgnString)
        game.preload(with: pgn)
        
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 0)
        game.moveForward()
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 1)
        game.moveForward()
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 2)
        game.moveForward()
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 3)
        game.moveForward()
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 4)
        game.moveForward()
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 5)
        game.moveForward()
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 5)
        game.moveForward()
        game.moveBack()
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 4)
        game.moveBack()
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 3)
        game.moveBack()
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 2)
        game.moveBack()
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 1)
        game.moveBack()
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 0)
        game.moveBack()
        XCTAssertEqual(game.getPlayedIndividualMovesCount(), 0)
    }
    
    func testChessMoveNumber() {
        let pgnString = "1. e4 e5 2. Nf3 d6 3. Ng1"
        let pgn = PGN(string: pgnString)
        game.preload(with: pgn)
        
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 1)
        game.moveForward()
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 1)
        game.moveForward()
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 1)
        game.moveForward()
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 2)
        game.moveForward()
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 2)
        game.moveForward()
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 3)
        game.moveForward()
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 3)
        game.moveBack()
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 2)
        game.moveBack()
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 2)
        game.moveBack()
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 1)
        game.moveBack()
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 1)
        game.moveBack()
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 1)
        game.moveBack()
        XCTAssertEqual(game.getCurrentChessMoveNumber(), 1)
    }

}
