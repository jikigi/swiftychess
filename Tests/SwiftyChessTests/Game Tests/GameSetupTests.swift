//
//  GameTests.swift
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 3/12/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import XCTest
@testable import SwiftyChess

class GameSetupTests: XCTestCase {
    
    var game: Game = Game()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // MARK: Pawn
    func testInitialPawns() {
        for file in File.FILE_A...File.FILE_H {
            let whiteCoordinate = Coordinate(file: file, rank: Rank.RANK_2)
            helpTestPawns(coordinate: whiteCoordinate, color: .white)
            let blackCoordinate = Coordinate(file: file, rank: Rank.RANK_7)
            helpTestPawns(coordinate: blackCoordinate, color: .black)
        }
    }
    
    private func helpTestPawns(coordinate: Coordinate, color: Color) {
        helpTestPieceExists(coordinate: coordinate)
        helpTestPieceColor(coordinate: coordinate, color: color)
        helpTestPieceType(coordinate: coordinate, piece: Pawn.self)
        let rankOffset: Int = color == .white ? 1 : -1
        let oneRankOver = coordinate.rank + rankOffset
        let twoRanksOver = coordinate.rank + (rankOffset*2)
        let possibleMoves = Coordinate(file: coordinate.file, rank: oneRankOver!).uint64Val | Coordinate(file: coordinate.file, rank: twoRanksOver!).uint64Val
        XCTAssertEqual(self.game.legalMoves(forPieceAt: coordinate), possibleMoves, "Initial pawn moves not equal")
    }
    
    // MARK: Rook
    func testInitialRooks() {
        let whiteRook1Coord = Coordinate(file: .FILE_A, rank: .RANK_1)
        helpTestRooks(coordinate: whiteRook1Coord, color: .white)
        let whiteRook2Coord = Coordinate(file: .FILE_H, rank: .RANK_1)
        helpTestRooks(coordinate: whiteRook2Coord, color: .white)
        let blackRook1Coord = Coordinate(file: .FILE_A, rank: .RANK_8)
        helpTestRooks(coordinate: blackRook1Coord, color: .black)
        let blackRook2Coord = Coordinate(file: .FILE_H, rank: .RANK_8)
        helpTestRooks(coordinate: blackRook2Coord, color: .black)
    }
    
    private func helpTestRooks(coordinate: Coordinate, color: Color) {
        helpTestPieceExists(coordinate: coordinate)
        helpTestPieceColor(coordinate: coordinate, color: color)
        helpTestPieceType(coordinate: coordinate, piece: Rook.self)
        XCTAssertEqual(self.game.legalMoves(forPieceAt: coordinate) , 0, "Initial rook possibleMoves should be 0")
    }
    
    // MARK: Bishop
    func testInitialBishops() {
        let whiteBishop1Coord = Coordinate(file: .FILE_C, rank: .RANK_1)
        helpTestBishops(coordinate: whiteBishop1Coord, color: .white)
        let whiteBishop2Coord = Coordinate(file: .FILE_F, rank: .RANK_1)
        helpTestBishops(coordinate: whiteBishop2Coord, color: .white)
        let blackBishop1Coord = Coordinate(file: .FILE_C, rank: .RANK_8)
        helpTestBishops(coordinate: blackBishop1Coord, color: .black)
        let blackBishop2Coord = Coordinate(file: .FILE_F, rank: .RANK_8)
        helpTestBishops(coordinate: blackBishop2Coord, color: .black)
    }
    
    private func helpTestBishops(coordinate: Coordinate, color: Color) {
        helpTestPieceExists(coordinate: coordinate)
        helpTestPieceColor(coordinate: coordinate, color: color)
        helpTestPieceType(coordinate: coordinate, piece: Bishop.self)
        XCTAssertEqual(self.game.legalMoves(forPieceAt: coordinate), 0, "Initial bishop possibleMoves should be 0")
    }
    
    // MARK: Knight
    func testInitialKnights() {
        let whiteKnight1Coord = Coordinate(file: .FILE_B, rank: .RANK_1)
        helpTestKnights(coordinate: whiteKnight1Coord, color: .white)
        let whiteKnight2Coord = Coordinate(file: .FILE_G, rank: .RANK_1)
        helpTestKnights(coordinate: whiteKnight2Coord, color: .white)
        let blackKnight1Coord = Coordinate(file: .FILE_B, rank: .RANK_8)
        helpTestKnights(coordinate: blackKnight1Coord, color: .black)
        let blackKnight2Coord = Coordinate(file: .FILE_G, rank: .RANK_8)
        helpTestKnights(coordinate: blackKnight2Coord, color: .black)
    }
    
    private func helpTestKnights(coordinate: Coordinate, color: Color) {
        helpTestPieceExists(coordinate: coordinate)
        helpTestPieceColor(coordinate: coordinate, color: color)
        helpTestPieceType(coordinate: coordinate, piece: Knight.self)
        
        let rankTwoOffset: Int = color == .white ? 2 : -2
        let leftCoordinate = Coordinate(file: (coordinate.file - 1)!, rank: (coordinate.rank + rankTwoOffset)!)
        let rightCoordinate = Coordinate(file: (coordinate.file + 1)!, rank: (coordinate.rank + rankTwoOffset)!)
        let possibleMoves = leftCoordinate.uint64Val | rightCoordinate.uint64Val
        XCTAssertEqual(self.game.legalMoves(forPieceAt: coordinate), possibleMoves, "Initial knight possibleMoves is not right")
    }
    
    // MARK: Queen
    func testInitialQueens() {
        let whiteQueenCoord = Coordinate(file: .FILE_D, rank: .RANK_1)
        helpTestQueens(coordinate: whiteQueenCoord, color: .white)
        let blackQueenCoord = Coordinate(file: .FILE_D, rank: .RANK_8)
        helpTestQueens(coordinate: blackQueenCoord, color: .black)
    }
    
    private func helpTestQueens(coordinate: Coordinate, color: Color) {
        helpTestPieceExists(coordinate: coordinate)
        helpTestPieceColor(coordinate: coordinate, color: color)
        helpTestPieceType(coordinate: coordinate, piece: Queen.self)
        
        XCTAssertEqual(self.game.legalMoves(forPieceAt: coordinate), 0, "Initial queen possibleMoves should be 0")
    }
    
    // MARK: King
    func testInitialKings() {
        let whiteKingCoord = Coordinate(file: .FILE_E, rank: .RANK_1)
        helpTestKings(coordinate: whiteKingCoord, color: .white)
        let blackKingCoord = Coordinate(file: .FILE_E, rank: .RANK_8)
        helpTestKings(coordinate: blackKingCoord, color: .black)
    }
    
    private func helpTestKings(coordinate: Coordinate, color: Color) {
        helpTestPieceExists(coordinate: coordinate)
        helpTestPieceColor(coordinate: coordinate, color: color)
        helpTestPieceType(coordinate: coordinate, piece: King.self)
        
        XCTAssertEqual(self.game.legalMoves(forPieceAt: coordinate), 0, "Initial king possibleMoves should be 0")
    }
    
    // MARK: Utility tests
    private func helpTestPieceExists(coordinate: Coordinate) {
        XCTAssertNotNil(self.game.piece(at: coordinate), "A peice should be initialized in file \(coordinate.file.rawValue) and rank \(coordinate.rank.rawValue)")
    }
    
    private func helpTestPieceColor(coordinate: Coordinate, color: Color) {
        XCTAssertEqual(self.game.piece(at: coordinate)!.color, color, "The piece at file \(coordinate.file.rawValue) and rank \(coordinate.rank.rawValue) should be \(color.toString())")
    }
    
    private func helpTestPieceType<T:ChessPiece>(coordinate: Coordinate, piece: T.Type) {
        XCTAssertTrue(self.game.piece(at: coordinate)! is T, "A piece of type \(String(describing: piece)) should be initialized in file \(coordinate.file.rawValue) and rank \(coordinate.rank.rawValue)")
    }
    
}
