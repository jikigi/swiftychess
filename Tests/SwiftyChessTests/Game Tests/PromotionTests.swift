//
//  PromotionTests.swift
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 7/29/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

import XCTest
@testable import SwiftyChess

class PromotionTests: XCTestCase {

    var game: Game = Game()
    
    override func setUp() {
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_2),
                       to: Coordinate(file: .FILE_E, rank: .RANK_4))
        
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_7),
                       to: Coordinate(file: .FILE_D, rank: .RANK_5))
        
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_4),
                       to: Coordinate(file: .FILE_E, rank: .RANK_5))
        
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_4),
                       to: Coordinate(file: .FILE_E, rank: .RANK_5))
        
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_4),
                       to: Coordinate(file: .FILE_E, rank: .RANK_5))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_7),
                       to: Coordinate(file: .FILE_F, rank: .RANK_6))
        
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_5),
                       to: Coordinate(file: .FILE_F, rank: .RANK_6))
        
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_7),
                       to: Coordinate(file: .FILE_E, rank: .RANK_5))
        
        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_6),
                       to: Coordinate(file: .FILE_G, rank: .RANK_7))
        
        self.game.move(from: Coordinate(file: .FILE_C, rank: .RANK_8),
                       to: Coordinate(file: .FILE_D, rank: .RANK_7))
    }

    override func tearDown() {
        self.game.reset()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testQueenPromotion() {
        helpTestG7Pawn()
        self.game.move(from: Coordinate(file: .FILE_G, rank: .RANK_7),
                       to: Coordinate(file: .FILE_F, rank: .RANK_8),
                       promotion: .queen)
        let queenPiece = self.game.piece(at: Coordinate(file: .FILE_F, rank: .RANK_8))
        XCTAssertNotNil(queenPiece, "The piece at f8 should not be nil")
        XCTAssertNil(self.game.piece(at: Coordinate(file: .FILE_G, rank: .RANK_7)), "There should not be a piece at g7")
        XCTAssertTrue(queenPiece! is Queen, "The piece at g7 should not be a pawn")
        XCTAssertTrue(self.game.isInCheck, "The other king should be in check")
        
        self.game.moveBack()
        helpTestG7Pawn()
        XCTAssertFalse(self.game.isInCheck, "The other king should be out of check")
    }
    
    func testRookPromotion() {
        helpTestG7Pawn()
        self.game.move(from: Coordinate(file: .FILE_G, rank: .RANK_7),
                       to: Coordinate(file: .FILE_F, rank: .RANK_8),
                       promotion: .rook)
        let rookPiece = self.game.piece(at: Coordinate(file: .FILE_F, rank: .RANK_8))
        XCTAssertNotNil(rookPiece, "The piece at f8 should not be nil")
        XCTAssertNil(self.game.piece(at: Coordinate(file: .FILE_G, rank: .RANK_7)), "There should not be a piece at g7")
        XCTAssertTrue(rookPiece! is Rook, "The piece at g7 should not be a pawn")
        XCTAssertTrue(self.game.isInCheck, "The other king should be in check")
        
        self.game.moveBack()
        helpTestG7Pawn()
        XCTAssertFalse(self.game.isInCheck, "The other king should be out of check")
    }
    
    func testBishopPromotion() {
        helpTestG7Pawn()
        self.game.move(from: Coordinate(file: .FILE_G, rank: .RANK_7),
                       to: Coordinate(file: .FILE_F, rank: .RANK_8),
                       promotion: .bishop)
        let bishopPiece = self.game.piece(at: Coordinate(file: .FILE_F, rank: .RANK_8))
        XCTAssertNotNil(bishopPiece, "The piece at f8 should not be nil")
        XCTAssertNil(self.game.piece(at: Coordinate(file: .FILE_G, rank: .RANK_7)), "There should not be a piece at g7")
        XCTAssertTrue(bishopPiece! is Bishop, "The piece at g7 should not be a pawn")
        XCTAssertFalse(self.game.isInCheck, "The other king should not be in check")
        
        self.game.moveBack()
        helpTestG7Pawn()
        XCTAssertFalse(self.game.isInCheck, "The other king should be out of check")
    }
    
    func testKnightPromotion() {
        helpTestG7Pawn()
        self.game.move(from: Coordinate(file: .FILE_G, rank: .RANK_7),
                       to: Coordinate(file: .FILE_F, rank: .RANK_8),
                       promotion: .knight)
        let knightPiece = self.game.piece(at: Coordinate(file: .FILE_F, rank: .RANK_8))
        XCTAssertNotNil(knightPiece, "The piece at f8 should not be nil")
        XCTAssertNil(self.game.piece(at: Coordinate(file: .FILE_G, rank: .RANK_7)), "There should not be a piece at g7")
        XCTAssertTrue(knightPiece! is Knight, "The piece at g7 should not be a pawn")
        XCTAssertFalse(self.game.isInCheck, "The other king should not be in check")
        
        self.game.moveBack()
        helpTestG7Pawn()
        XCTAssertFalse(self.game.isInCheck, "The other king should be out of check")
    }

    
    func helpTestG7Pawn() {
        let pawnPiece = self.game.piece(at: Coordinate(file: .FILE_G, rank: .RANK_7))
        XCTAssertNotNil(pawnPiece, "The piece at g7 should not be nil")
        XCTAssertTrue(pawnPiece! is Pawn, "The piece at g7 should be a pawn")
    }
}
