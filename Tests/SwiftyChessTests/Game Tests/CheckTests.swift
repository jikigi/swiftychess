//
//  CheckTests.swift
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 11/30/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import XCTest
import SwiftyChess

class CheckTests: XCTestCase {

    var game: Game!
    
    override func setUp() {
        self.game = Game()
    }
    
    func testBasicCheck() {
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_2),
                       to: Coordinate(file: .FILE_D, rank: .RANK_3))
        
        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_7),
                       to: Coordinate(file: .FILE_E, rank: .RANK_6))
        
        // random move
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_2),
                       to: Coordinate(file: .FILE_H, rank: .RANK_3))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_8),
                       to: Coordinate(file: .FILE_B, rank: .RANK_4))
        
        XCTAssertTrue(self.game.isInCheck, "White should be in check")
    }

    func testCheckPossibleMoves() {
        self.game.move(from: Coordinate(file: .FILE_D, rank: .RANK_2),
                       to: Coordinate(file: .FILE_D, rank: .RANK_3))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_7),
                       to: Coordinate(file: .FILE_E, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_2),
                       to: Coordinate(file: .FILE_A, rank: .RANK_3))

        // random move
        self.game.move(from: Coordinate(file: .FILE_H, rank: .RANK_7),
                       to: Coordinate(file: .FILE_H, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_E, rank: .RANK_2),
                       to: Coordinate(file: .FILE_E, rank: .RANK_3))

        self.game.move(from: Coordinate(file: .FILE_F, rank: .RANK_8),
                       to: Coordinate(file: .FILE_B, rank: .RANK_4))

        let whiteA3PawnPossibleMoves = Coordinate(file: .FILE_B, rank: .RANK_4).uint64Val
        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_A, rank: .RANK_3)),
                       whiteA3PawnPossibleMoves,
                       "The pawn at A3 should only be able to take the piece that is checking the king")
        
        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_B, rank: .RANK_2)),
                       0,
                       "The pawn at B2 should not be able to move")

        let whiteB1KnightPossibleMoves = Coordinate(file: .FILE_C, rank: .RANK_3).uint64Val |
            Coordinate(file: .FILE_D, rank: .RANK_2).uint64Val
        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_B, rank: .RANK_1)),
                       whiteB1KnightPossibleMoves,
                       "The knight at B1 should only be able to block the check")

        let whiteC2PawnPossibleMoves = Coordinate(file: .FILE_C, rank: .RANK_3).uint64Val
        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_C, rank: .RANK_2)),
                       whiteC2PawnPossibleMoves,
                       "The pawn at C2 should only be able to block the check")

        let whiteQueenPossibleMoves = Coordinate(file: .FILE_D, rank: .RANK_2).uint64Val
        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_D, rank: .RANK_1)),
                       whiteQueenPossibleMoves,
                       "The queen at D1 should only be able to block the check")

        let whiteKingPossibleMoves = Coordinate(file: .FILE_E, rank: .RANK_2).uint64Val
        
        XCTAssertEqual(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_E, rank: .RANK_1)),
                       whiteKingPossibleMoves,
                       "The king can only move to E2 because it is the only square where the king is not in check")
    }
    
//    func testDoubleCheck() {
//
//    }
//
//    func testCheckWithSkewer() {
//
//    }
    
}
