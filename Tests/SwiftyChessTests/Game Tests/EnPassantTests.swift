//
//  EnPassantTests.swift
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 11/27/18.
//  Copyright © 2018 Casey Evanish. All rights reserved.
//

import XCTest
@testable import SwiftyChess

class EnPassantTests: XCTestCase {

    var game: Game!
    
    override func setUp() {
        self.game = Game()
    }
    
    func testWhiteEnpassant() {
        for file in File.FILE_A...File.FILE_H {
            self.game.move(from: Coordinate(file: file, rank: .RANK_2),
                           to: Coordinate(file: file, rank: .RANK_5))
            if let leftFile = file - 1 {
                
                let enpassantCoord = Coordinate(file: leftFile, rank: .RANK_6)
                self.game.move(from: Coordinate(file: leftFile, rank: .RANK_7), to: Coordinate(file: leftFile, rank: .RANK_5))
                XCTAssertTrue(self.game.legalMoves(forPieceAt: Coordinate(file: file, rank: .RANK_5)).contains(enpassantCoord.uint64Val))
            }
            if let rightFile = file + 1 {
                self.game.move(from: Coordinate(file: rightFile, rank: .RANK_7),
                               to: Coordinate(file: rightFile, rank: .RANK_5))
                let enpassantCoord = Coordinate(file: rightFile, rank: .RANK_6)
                XCTAssertTrue(self.game.legalMoves(forPieceAt: Coordinate(file: file, rank: .RANK_5)).contains(enpassantCoord.uint64Val))
            }
            self.game.reset()
        }
    }
    
    func testBlackEnpassant() {
        for file in File.FILE_A...File.FILE_H {
            self.game.move(from: Coordinate(file: file, rank: .RANK_7),
                           to: Coordinate(file: file, rank: .RANK_4))
            if let leftFile = file - 1 {
                self.game.move(from: Coordinate(file: leftFile, rank: .RANK_2),
                               to: Coordinate(file: leftFile, rank: .RANK_4))
                
                let enpassantCoord = Coordinate(file: leftFile, rank: .RANK_3)
                XCTAssertTrue(self.game.legalMoves(forPieceAt: Coordinate(file: file, rank: .RANK_4)).contains(enpassantCoord.uint64Val))
            }

            if let rightFile = file + 1 {
                self.game.move(from: Coordinate(file: rightFile, rank: .RANK_2),
                               to: Coordinate(file: rightFile, rank: .RANK_4))
                let enpassantCoord = Coordinate(file: rightFile, rank: .RANK_3)
                XCTAssertTrue(self.game.legalMoves(forPieceAt: Coordinate(file: file, rank: .RANK_4)).contains(enpassantCoord.uint64Val))
            }
            self.game.reset()
        }
    }

    // should not allow enpassant if the pawn is on the 6th rank and the opposing pawn advances two
    func testNotAllowedEnpassantWhenPawnIsIn6thRank() {
        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_2),
                       to: Coordinate(file: .FILE_A, rank: .RANK_6))

        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_7),
                       to: Coordinate(file: .FILE_B, rank: .RANK_5))

        XCTAssertFalse(self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_A, rank: .RANK_6)).contains(Coordinate(file: .FILE_B, rank: .RANK_7).uint64Val))
    }

    // should not allow enpassant if more than one turn passed
    func testNotAllowedEnpassantWhenMoreThanOneTurnPassed() {
        self.game.move(from: Coordinate(file: .FILE_B, rank: .RANK_2),
                       to: Coordinate(file: .FILE_B, rank: .RANK_5))

        self.game.move(from: Coordinate(file: .FILE_C, rank: .RANK_7),
                       to: Coordinate(file: .FILE_C, rank: .RANK_5))

        var legalMoves = self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_B, rank: .RANK_5))
        XCTAssertTrue(legalMoves.contains(Coordinate(file: .FILE_C, rank: .RANK_6).uint64Val))
        XCTAssertFalse(legalMoves.contains(Coordinate(file: .FILE_A, rank: .RANK_6).uint64Val))

        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_2),
                       to: Coordinate(file: .FILE_A, rank: .RANK_3))

        self.game.move(from: Coordinate(file: .FILE_A, rank: .RANK_7),
                       to: Coordinate(file: .FILE_A, rank: .RANK_6))
        legalMoves = self.game.legalMoves(forPieceAt: Coordinate(file: .FILE_B, rank: .RANK_5))
        XCTAssertFalse(legalMoves.contains(Coordinate(file: .FILE_C, rank: .RANK_6).uint64Val))
        XCTAssertTrue(legalMoves.contains(Coordinate(file: .FILE_A, rank: .RANK_6).uint64Val))
    }

}
