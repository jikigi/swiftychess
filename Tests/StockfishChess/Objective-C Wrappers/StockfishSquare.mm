//
//  StockfishSquare.m
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 5/7/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

#import "StockfishSquare.h"
#include "../Chess Logic/square.h"

@interface StockfishSquare()
@end

@implementation StockfishSquare

+(NSString *) toString: (Square) square {
    return [NSString stringWithCString:Chess::square_to_string(((Chess::Square) square)).c_str()
                              encoding:[NSString defaultCStringEncoding]];
}

@end
