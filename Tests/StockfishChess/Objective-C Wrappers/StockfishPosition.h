//
//  position.h
//  ChessOpenings
//
//  Created by Casey Evanish on 5/7/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "StockfishSquare.h"
#import "StockfishPromotionType.h"

typedef NS_ENUM(NSUInteger, Move) {
    MOVE_NONE = 0,
    MOVE_NULL = 65,
    MOVE_MAX = 0xFFFFFF
};

@interface StockfishPosition : NSObject

- (nonnull instancetype) initWithFen:(nonnull NSString *) fen;
- (nonnull NSArray<NSNumber *> *) destinationSquaresFrom:(Square) s;
- (Move)doMoveFrom:(Square)fSq to:(Square)tSq promotion:(PromotionType)prom;
- (void)takeBack;

@end
