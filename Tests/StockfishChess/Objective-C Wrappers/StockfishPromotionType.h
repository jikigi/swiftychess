//
//  StockfishPieceType.h
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 8/11/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, PromotionType) {
    NO_PIECE_TYPE = 0, KNIGHT = 2, BISHOP = 3, ROOK = 4, QUEEN = 5
};
