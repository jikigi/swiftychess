//
//  position.m
//  ChessOpeningsTests
//
//  Created by Casey Evanish on 5/7/19.
//  Copyright © 2019 Casey Evanish. All rights reserved.
//

#import "StockfishPosition.h"

#include "../Chess Logic/position.h"
#include "../Chess Logic/square.h"
#include "../Chess Logic/color.h"
#include "../Chess Logic/bitboard.h"
#include "../Chess Logic/mersenne.h"
#include "../Chess Logic/movepick.h"
#include "../Chess Logic/move.h"
#include "ChessMove.h"

@interface StockfishPosition ()
@property (nonatomic, readwrite) Chess::Position* position;
@property (nonatomic, readwrite) NSMutableArray<ChessMove *> *moves;
@property (nonatomic, readwrite) int currentMoveIndex;
@end

@implementation StockfishPosition

-(instancetype) initWithFen:(NSString *)fen {
    self = [super init];
    if(self != nil)
    {
        /* Chess init */
        Chess::init_mersenne();
        Chess::init_direction_table();
        Chess::init_bitboards();
        Chess::Position::init_zobrist();
        Chess::Position::init_piece_square_tables();
        Chess::MovePicker::init_phase_table();
        _position = new Chess::Position;
        _position -> from_fen([fen UTF8String]);
        _currentMoveIndex = 0;
        _moves = [[NSMutableArray alloc] init];;
    }
    return self;
}

-(NSArray<NSNumber *> *) destinationSquaresFrom:(Square)s {
    NSMutableArray<NSNumber *> *returnVal = [[NSMutableArray alloc] init];
    Chess::Square sqs[32];
    [self destinationSquaresFrom:(Chess::Square)s saveInArray:sqs];
    for(int i = 0; sqs[i] != Chess::SQ_NONE; i++) {
        [returnVal addObject: [[NSNumber alloc] initWithUnsignedInteger:(Square)sqs[i]]];
    }
    return returnVal;
}

- (Move)doMoveFrom:(Square)fSq to:(Square)tSq promotion:(PromotionType)prom {
    
    // Find the matching move
    Chess::Move mlist[32], move = Chess::MOVE_NONE;
    int n, i, matches;
    if(_position -> type_of_piece_on((Chess::Square)fSq) == Chess::PieceType::KING &&
        Chess::file_distance((Chess::Square)fSq, (Chess::Square)tSq) == 2) {
        switch (tSq) {
            case SQ_C8:
                tSq = SQ_A8;
                break;
            case SQ_C1:
                tSq = SQ_A1;
                break;
            case SQ_G1:
                tSq = SQ_H1;
                break;
            case SQ_G8:
                tSq = SQ_H8;
                break;
            default:
                break;
        }
    }
    Chess::PieceType stockfishProm;
    switch (prom) {
        case NO_PIECE_TYPE:
            stockfishProm = Chess::PieceType::NO_PIECE_TYPE;
            break;
        case BISHOP:
            stockfishProm = Chess::PieceType::BISHOP;
            break;
        case KNIGHT:
            stockfishProm = Chess::PieceType::KNIGHT;
            break;
        case ROOK:
            stockfishProm = Chess::PieceType::ROOK;
            break;
        case QUEEN:
            stockfishProm = Chess::PieceType::QUEEN;
            break;
    }
    
    n = _position->moves_from((Chess::Square)fSq, mlist);
    for (i = 0, matches = 0; i < n; i++)
        if (move_to(mlist[i]) == (Chess::Square)tSq && move_promotion(mlist[i]) == stockfishProm) {
            move = mlist[i];
            matches++;
        }
    assert(matches == 1);
    
    // Update position
    Chess::UndoInfo u;
    _position->do_move(move, u);
    ChessMove *cm = [[ChessMove alloc] initWithMove: move undoInfo: u];
    
    [_moves addObject:cm];
    _currentMoveIndex++;
    return (Move)move;
}

- (void)takeBack {
    if (_currentMoveIndex > 0) {
        _currentMoveIndex--;
        ChessMove *cm = self.moves[_currentMoveIndex];
        Chess::Move m = [cm move];
        Chess::UndoInfo u = [cm undoInfo];
        _position->undo_move(m, u);
    }
}

- (int)destinationSquaresFrom:(Chess::Square)sq saveInArray:(Chess::Square *)sqs {
    int i, j, n;
    Chess::Move mlist[32];
    
    n = _position -> moves_from(sq, mlist);
    for (i = 0, j = 0; i < n; i++)
        // Only include non-promotions and queen promotions, in order to avoid
        // having the same destination squares multiple times in the array.
        if (!move_promotion(mlist[i]) || move_promotion(mlist[i]) == Chess::QUEEN) {
            // For castling moves, adjust the destination square so that it displays
            // correctly when squares are highlighted in the GUI.
            if (move_is_long_castle(mlist[i]))
                sqs[j] = move_to(mlist[i]) + 2;
            else if (move_is_short_castle(mlist[i]))
                sqs[j] = move_to(mlist[i]) - 1;
            else
                sqs[j] = move_to(mlist[i]);
            j++;
        }
    sqs[j] = Chess::Square::SQ_NONE;
    return j;
}
@end
