// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftyChess",
    platforms: [.iOS("13.0"), .macOS("10.13")],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SwiftyChess",
            targets: ["SwiftyChess"]),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SwiftyChess",
            dependencies: []),
        .target(
            name: "StockfishChess",
            dependencies: [],
            path: "Tests/StockfishChess"),
        .testTarget(
            name: "SwiftyChessTests",
            dependencies: ["SwiftyChess", "StockfishChess"],
            resources: [
                .process("Resources")
            ]),
    ]
)
